# Miners Minerals

* A mod for Minecraft.
* Requires [EzModLib](https://gitlab.com/ZephaniahNoah/ez-mod-lib).
* Download the mod from [CurseForge](https://www.curseforge.com/minecraft/mc-mods/miners-minerals) or [Modrinth](https://modrinth.com/mod/miners-minerals).
