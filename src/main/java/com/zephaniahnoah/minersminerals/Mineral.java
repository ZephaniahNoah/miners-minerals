package com.zephaniahnoah.minersminerals;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.IItemTier;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.LazyValue;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.biome.Biome.Category;

// TODO: Phoenixite
public enum Mineral implements IItemTier, IArmorMaterial, IMineralOre {
	// 1 2 3 4 5 6 7 8 9 10 11 12 13
    //        lv uses  speed damag ench tuf knockback   armor protection   minD maxD min max freq size      baseBlocks   biomes
	//         level
	//         | uses                         slotProtections               minDrops               size
	//         |  |   speed                          |                         |maxDrops            | baseBlocks
	//         |  |     |   damage                   |                         |  |minHeight        |    |
	//         |  |     |     |  enchantability      |                         |  |  | maxHeight    |    |               biomes
	//         |  |     |     |     | toughness      |                         |  |  |    |frequency|    |                  |
	//         |  |     |     |     |    | knockback |                         |  |  |    |    |    |    |                  |
	//         |  |     |     |     |    |  |        |                         |  |  |    |    |    |    |                  |

	// All minerals that can have ores spawn in them must be initialized first so that they actually exist when ores get created
	RHODONITE  (2, 350, 5.0F, 1.5F, 10,  0, 0,      new int[] { 2, 3, 3,  2},  1, 1, 0,   62,  1, 250, Ores.defaultBlocks, null), //
	JET        (1, 350, 4.2F, 1.3F, 10,  0, 0,      new int[] { 1, 2, 3, 2 },  1, 1, 50,  200, 1, 250, Ores.defaultBlocks, null), //
	SERPENTINE (2, 120, 5.5F, 1.8F, 10,  0, 0,      new int[] { 1, 2, 3, 2 },  1, 1, 0,   200, 1, 250, Ores.defaultBlocks, new Category[] {Category.JUNGLE}), //
	BLOOD_STONE(1, 350, 4.2F, 1.4F, 10,  0, 0,      new int[] { 1, 2, 3, 2 },  1, 1, 0,   200, 1, 250, Ores.defaultBlocks, new Category[] {Category.NETHER}), //
	CHAROITE   (1, 850, 5.3F, 1.2F, 22,  0, 0.1F,   new int[] { 1, 2, 3, 2 },  1, 1, 24,  44,  1, 250, Ores.defaultBlocks, null), //
	BRONZITE   (1, 850, 4.0F, 1.5F, 22,  0, 0.1F,   new int[] { 1, 2, 3, 1 },  1, 1, 10,  30,  1, 250, Ores.defaultBlocks, null), //

	COPPER    (1, 200,  4.7F, 1.5F, 10,  0, 0,      new int[] { 1, 2, 3,  2 }, 0, 0, 59,  60,  10,  8,  Ores.defaultBlocks, null), //
	TIN       (1, 120,  4.2F, 1.1F, 7,   0, 0,      new int[] { 1, 2, 3,  1 }, 0, 0, 40,  63,  8,   7,  Ores.defaultBlocks, null), //
	ALUMINUM  (1, 160,  5.6F, 1.6F, 9,   0, 0,      new int[] { 1, 2, 2,  1 }, 0, 0, 40,  60,  19,  11, new Block[] { Blocks.DIRT }, null), //
	NICKEL    (2, 225,  5.8F, 1.8F, 10,  0, 0,      new int[] { 1, 3, 4,  2 }, 0, 0, 45,  55,  5,   6,  Ores.defaultBlocks, null), //
	BRASS     (2, 240,  6.1F, 2.1F, 12,  0, 0,      new int[] { 1, 2, 4,  2 }, 0, 0, 0,   0,   0,   0,  Ores.defaultBlocks, null), //
	BRONZE    (2, 400,  6.2F, 2.2F, 13,  0, 0,      new int[] { 2, 4, 5,  3 }, 0, 0, 0,   0,   0,   0,  Ores.defaultBlocks, null), //
	LEAD      (2, 200,  6.1F, 2.1F, 8,   0, 0.25F,  new int[] { 2, 4, 6,  2 }, 0, 0, 36,  42,  4,   7,  Ores.defaultBlocks, null), //
	SILVER    (2, 220,  6.4F, 2.3F, 16,  0, 0,      new int[] { 2, 5, 6,  3 }, 0, 0, 36,  42,  7,   2,  Ores.defaultBlocks, null), //
	TITANIUM  (3, 1100, 7.1F, 2.0F, 14,  0, 0,      new int[] { 3, 7, 9,  4 }, 0, 0, 5,   18,  5,   1,  Ores.defaultBlocks, null), //
	TUNGSTEN  (3, 900,  6.3F, 2.0F, 13,  0, 0.1F,   new int[] { 2, 5, 6,  2 }, 0, 0, 24,  44,  7,   5,  Ores.defaultBlocks, null), //
	PLATINUM  (3, 900,  6.5F, 2.5F, 17,  0, 0,      new int[] { 3, 5, 7,  3 }, 0, 0, 10,  30,  3,   5,  Ores.defaultBlocks, null), //
	ELECTRUM  (2, 350,  5.0F,  2.4F, 25, 0, 0,      new int[] { 2, 5, 6,  2 }, 0, 0, 28,  32,  1,   3,  Ores.defaultBlocks, null), //
	ADAMANTIUM(2, 430,  9.0F,  5.5F, 15, 0, 0.05F,  new int[] { 4, 8, 10, 5 }, 0, 0, 0,   30,  2,   3,  new Supplier[] { ()->Main.baseMinerals.swap().get(BRONZITE)}, null), //
	OSMIUM    (2, 350,  6.7F,  2.5F, 8,  0, 0.05F,  new int[] { 1, 2, 3,  2 }, 0, 0, 16,  24,  6,   4,  Ores.defaultBlocks, null), //
	STEEL     (2, 400,  7.0F,  2.5F, 10, 0, 0,      new int[] { 2, 6, 7,  3 }, 0, 0, 0,   0,   0,   0,  Ores.defaultBlocks, null), //
	PALLADIUM (3, 200,  8.5F,  3.5F, 12, 0, 0,      new int[] { 2, 4, 5,  2 }, 0, 0, 5,   20,  3,   2,  Ores.defaultBlocks, null), //
	COBALT    (3, 2350, 10.0F, 3.5F, 15, 2, 0.1F,   new int[] { 4, 8, 10, 5 }, 0, 0, 5,   20,  1,   4,  Ores.defaultBlocks, null), // TODO I want it to spawn in a cool way but idk yet
	MYTHRIL   (4, 1350, 11.7F, 3.5F, 20, 1, 0.1F,   new int[] { 3, 7, 9,  4 }, 0, 0, 0,   20,  1,   2,  Ores.defaultBlocks, null), //
	ETHERIUM  (2, 550,  4.7F,  3.5F, 17, 0, 0,      new int[] { 3, 7, 9,  4 }, 0, 0, 0,   20,  10,  3,  new Block[] { Blocks.END_STONE }, new Category[] {Category.THEEND}), //
	ORICHALCUM(3, 400,  8.7F,  2.5F, 30, 0, 0,      new int[] { 2, 6, 7,  3 }, 0, 0, 120, 150, 7,   3,  Ores.defaultBlocks, null), //
	MAGNETITE (2, 350,  7.7F,  2.2F, 11, 0, 0,      new int[] { 2, 4, 6,  3 }, 0, 0, 0,   200, 8,   3,  Ores.defaultBlocks, null), // TODO (Wearing the armor increases your item pickup range and tools add the item colected straght to inventory)
	BISMUTH   (2, 225,  6.8F,  2.2F, 22, 0, 0,      new int[] { 2, 4, 6,  2 }, 0, 0, 40,  65,  5,   3,  Ores.defaultBlocks, null), //
	SILICON   (3, 230,  7.5F,  2.4F, 16, 0, 0,      new int[] { 3, 4, 5,  3 }, 0, 0, 0,   25,  80,  10, new Block[] { Blocks.DIRT, Blocks.GRAVEL }, null), //
	ZINC      (2, 190,  5.0F,  1.6F, 6,  0, 0.05F,  new int[] { 2, 4, 5,  2 }, 0, 0, 40,  60,  15,  4,  Ores.defaultBlocks, null), //
	ANTIMONY  (2, 175,  4.7F,  2.2F, 14, 0, 0,      new int[] { 3, 4, 5,  3 }, 0, 0, 0,   150, 120, 1,  Ores.defaultBlocks, null), //

	//         lv uses  speed damag ench tuf knockback   armor protection   minD maxD min max freq size      baseBlocks   biomes
	//          level
	//          | uses                         slotProtections         minDrops               size
	//          |  |   speed                          |                   |maxDrops            | baseBlocks
	//          |  |     |   damage                   |                   |  |minHeight        |    |
	//          |  |     |     |  enchantability      |                   |  |  | maxHeight    |    |               biomes
	//          |  |     |     |     | toughness      |                   |  |  |    |frequency|    |                  |
	//          |  |     |     |     |    | knockback |                   |  |  |    |    |    |    |                  |
	//          |  |     |     |     |    | |         |                   |  |  |    |    |    |    |                  |
	//PHOENIXITE (2, 1200,  10F, 3.0F, 15, 0, 0,  new int[] { 3, 4, 4, 3 }, 1, 1, 0,   15,  2,   2,  new Supplier[] { ()->Blocks.NETHERRACK.getRegistryName(), ()->Main.baseMinerals.swap().get(BLOOD_STONE)}, null), //
	SPINEL     (2, 1000, 8.3F, 3.2F, 10, 0, 0,  new int[] { 2, 5, 6, 3 }, 1, 1, 30,  200, 3,   3,  Ores.defaultBlocks, null), //
	TOURMALINE (2, 750,  9.5F, 2.8F, 10, 0, 0,  new int[] { 3, 5, 7, 3 }, 1, 1, 0,   10,  6,   6,  Ores.defaultBlocks, null), //
	TIGERS_EYE (2, 350,  6.5F, 1.9F, 22, 0, 0,  new int[] { 2, 5, 6, 3 }, 1, 1, 0,   200, 30,  5,  Ores.defaultBlocks, new Category[] {Category.SAVANNA}), //
	PERIDOT    (2, 350,  6.1F, 2.1F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 0,   200, 15,  3,  Ores.defaultBlocks, null), //
	OPAL       (1, 350,  5.8F, 2.3F, 26, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 50,  150, 33,  3,  new Block[] { Blocks.SANDSTONE, Blocks.RED_SANDSTONE, Blocks.YELLOW_TERRACOTTA, Blocks.BROWN_TERRACOTTA, Blocks.RED_TERRACOTTA, Blocks.WHITE_TERRACOTTA, Blocks.LIGHT_GRAY_TERRACOTTA, Blocks.ORANGE_TERRACOTTA}, new Category[] {Category.DESERT,Category.MESA}), //
	JASPER     (2, 450,  9.7F, 2.5F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 70,  80,  2,   3,  Ores.defaultBlocks, null), //
	SELENITE   (2, 210,  4.5F, 1.6F, 22, 1, 0,  new int[] { 1, 2, 3, 1 }, 1, 4, 19,  75,  25,  8,  Ores.defaultBlocks, null), //
	GARNET     (2, 350,  5.7F, 2.5F, 25, 0, 0,  new int[] { 2, 4, 6, 2 }, 1, 1, 0,   128, 30,  4,  new Supplier[] { ()->Blocks.NETHERRACK.getRegistryName(), ()->Main.baseMinerals.swap().get(BLOOD_STONE)}, null), //
	MOSS_AGATE (2, 750,  6.7F, 3.4F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 0,   200, 25,  6,  Ores.defaultBlocks, new Category[] {Category.JUNGLE}), //
	JADE       (1, 450,  5.1F, 1.7F, 10, 0, 0,  new int[] { 2, 4, 5, 3 }, 1, 4, 0,   30,  30,  8,  Ores.defaultBlocks, new Category[] {Category.JUNGLE}), //
	RUBY       (2, 350,  6.4F, 2.5F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 0,   62,  3,   3,  Ores.defaultBlocks, null), //
	AMETHYST   (2, 350,  5.7F, 2.5F, 10, 0,.02F,new int[] { 1, 2, 3, 2 }, 1, 3, 20,  40,  2,   9,  Ores.defaultBlocks, null), //
	SAPPHIRE   (2, 350,  6.7F, 2.4F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 0,   30,  10,  3,  Ores.defaultBlocks, null), //
	QUARTZ     (2, 350,  5.8F, 1.8F, 9,  0, 0,  new int[] { 2, 3, 4, 2 }, 1, 1, 0,   0,   0,   0,  Ores.defaultBlocks, null), //
	ZIRCON     (2, 350,  5.7F, 2.9F, 10, 0, 0,  new int[] { 3, 4, 5, 3 }, 1, 2, 20,  40,  2,   3,  Ores.defaultBlocks, null), //
	PRISMARINE (2, 350,  5.7F, 2.4F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 0,   200, 30,  3,  Ores.defaultBlocks, new Category[] {Category.OCEAN}), //
	SUNSTONE   (2, 550,  7.7F, 2.2F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 115, 128, 2,   2,  new Supplier[] { ()->Blocks.NETHERRACK.getRegistryName(), ()->Main.baseMinerals.swap().get(BLOOD_STONE)}, new Category[] {Category.NETHER}), //
	TOPAZ      (2, 75,   6.1F, 2.1F, 10, 0, 0,  new int[] { 2, 4, 6, 2 }, 1, 1, 0,   70,  3,   25, Ores.defaultBlocks, new Category[] {Category.NETHER}), //
	OLIVINE    (3, 900,  6.4F, 2.6F, 10, 0, 0,  new int[] { 3, 5, 7, 3 }, 1, 1, 0,   128, 30,  6,  new Block[] { Blocks.BASALT, Blocks.BLACKSTONE }, null), //
	AMBER      (1, 125,  4.5F, 3.5F, 10, 0, 0,  new int[] { 1, 3, 4, 2 }, 1, 1, 61,  80,  25,  2,  new Block[] { Blocks.SAND, Blocks.GRAVEL, Blocks.RED_SAND }, new Category[] {Category.BEACH}), //
	DEUTERIUM  (3, 500,  7.2F, 3.5F, 10, 0, 0,  new int[] { 4, 5, 7, 4 }, 1, 1, 50,  150,  5,  2,  new Block[] { Blocks.BLUE_ICE, Blocks.PACKED_ICE}, new Category[] {Category.ICY,Category.OCEAN}), //
	URANIUM    (3, 850,  6.3F, 2.2F, 22, 0,0.1F,new int[] { 2, 5, 6, 2 }, 0, 0, 24,  44,   4,  5,  Ores.defaultBlocks, null), //
	ONYX       (2, 350,  6.7F, 2.4F, 10, 0, 0,  new int[] { 1, 2, 3, 2 }, 1, 1, 0,   30,   3,  7,  Ores.defaultBlocks, null), //
	HEDERAIUM  (3, 1000, 7.3F, 4.5F, 33, 0,0.1F,new int[] { 3, 4, 8, 3 }, 0, 0, 12,  44,   1,  5,  Ores.defaultBlocks, new Category[] {Category.SWAMP}), //
	SHROOMITE  (3, 1100, 8.3F, 3.5F, 33, 0,0.1F,new int[] { 3, 4, 8, 3 }, 0, 0, 24,  44,   7,  5,  Ores.defaultBlocks, new Category[] {Category.MUSHROOM}); //
	// Idea. Gold plated netherite armor (Increases enchantability and make piglins like you)
	// Dragon drops a scale which be used to upgrade netherite armor
	// Make some minerals usable as beacon base
	// New mineral that spawns in ice???

	// CELESTINE
	// NEPTUNITE
	// MOONSTONE
	// AQUAMARINE
	// AGATE
	// ONYX
	// ALEXANDRITE
	// TURQUOISE
	// NACRE
	// AMMOLITE

	private final int level;
	private final int uses;
	private final int enchantmentValue;
	private final float speed;
	private final float damage;
	private float toughness;
	private float knockback;
	private int[] slotProtections;
	public final String tag;
	private final LazyValue<Ingredient> repairIngredient;
	public final int minDrops, maxDrops, minHeight, maxHeight, veinsPerChunk, size;
	public List<ResourceLocation> baseBlocks;
	private Supplier<?>[] baseBlocksUncooked;
	private Category[] biomes;

	private Mineral(int level, int uses, float speed, float damage, int enchantability, float toughness, float knockback, int[] slotProtections, int minDrops, int maxDrops, int minHeight, int maxHeight, int veinsPerChunk, int size, Object baseBlocks, Category[] biomes) {
		this.level = level;
		this.uses = uses;
		this.speed = speed;
		this.damage = damage;
		this.enchantmentValue = enchantability;
		this.toughness = toughness;
		this.knockback = knockback;
		this.slotProtections = slotProtections;
		this.minDrops = minDrops;
		this.maxDrops = maxDrops;
		this.tag = "forge:" + (gem() ? "gems" : "ingots") + "/" + getName();
		this.repairIngredient = new LazyValue<>(() -> {
			return Ingredient.of(ItemTags.bind(tag));
		});
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
		this.veinsPerChunk = veinsPerChunk;
		this.size = size;
		if (baseBlocks instanceof List) {
			this.baseBlocks = (List<ResourceLocation>) baseBlocks;
		} else if (baseBlocks instanceof Block[]) {
			this.baseBlocks = new ArrayList<ResourceLocation>();
			for (Block b : (Block[]) baseBlocks) {
				this.baseBlocks.add(b.getRegistryName());
			}
		} else {
			this.baseBlocksUncooked = (Supplier[]) baseBlocks;
		}
		this.biomes = biomes;
	}

	public void cook() {
		if (baseBlocksUncooked != null) {
			baseBlocks = new ArrayList<ResourceLocation>();
			for (Supplier<?> s : baseBlocksUncooked) {
				baseBlocks.add((ResourceLocation) s.get());
			}
		}
	}

	public boolean gem() {
		return minDrops != 0;
	}

	@Override
	public int getUses() {
		return this.uses;
	}

	@Override
	public float getSpeed() {
		return this.speed;
	}

	@Override
	public float getAttackDamageBonus() {
		return this.damage;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public int getEnchantmentValue() {
		return this.enchantmentValue;
	}

	public int func_200900_a() {
		return this.enchantmentValue;
	}

	@Override
	public Ingredient getRepairIngredient() {
		return this.repairIngredient.get();
	}

	public Ingredient func_200898_c() {
		return this.repairIngredient.get();
	}

	@Override
	public String getLowerName() {
		return name().toLowerCase();
	}

	@Override
	public int getDurabilityForSlot(EquipmentSlotType slotType) {
		int slot = 0;
		switch (slotType.getIndex()) {
		case 0:
			slot = 1;
			break;
		case 1:
			slot = 2;
			break;
		case 2:
			slot = 3;
			break;
		default:
			slot = 2;
		}
		return slot * uses;
	}

	@Override
	public int getDefenseForSlot(EquipmentSlotType slotType) {
		return this.slotProtections[slotType.getIndex()];
	}

	@Override
	public SoundEvent getEquipSound() {
		return SoundEvents.ARMOR_EQUIP_IRON;
	}

	@Override
	public float getToughness() {
		return toughness;
	}

	@Override
	public float getKnockbackResistance() {
		return knockback;
	}

	@Override
	public Category[] biomes() {
		return biomes;
	}

	@Override
	public String getName() {
		return getLowerName();
	}
}