package com.zephaniahnoah.minersminerals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;

public class Ores {
	public static List<ResourceLocation> defaultBlocks = new ArrayList<ResourceLocation>(Arrays.asList(new ResourceLocation[] { new ResourceLocation("minecraft:stone"), new ResourceLocation("minecraft:granite"), new ResourceLocation("minecraft:andesite"), new ResourceLocation("minecraft:diorite") }));
	public static HashMap<Mineral, Block[]> ores = new HashMap<Mineral, Block[]>();

	public static void register(Mineral ore, Block base, int index) {
		if (ores.get(ore) == null) {
			Block[] blocks = new Block[ore.baseBlocks.size()];
			blocks[index] = base;
			ores.put(ore, blocks);
		} else {
			for (int i = 0; i < ore.baseBlocks.size(); i++) {
				ores.get(ore)[index] = base;
			}
		}
	}
}
