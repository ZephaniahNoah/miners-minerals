package com.zephaniahnoah.minersminerals.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.zephaniahnoah.minersminerals.extras.shuriken.Shuriken;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

@Mixin(ItemRenderer.class)
public abstract class RenderItemOverlayIntoGUIMixin {

	private static final Minecraft mc = Minecraft.getInstance();

	@Inject(at = @At("HEAD"), method = "Lnet/minecraft/client/renderer/ItemRenderer;renderGuiItem(Lnet/minecraft/item/ItemStack;IILnet/minecraft/client/renderer/model/IBakedModel;)V", cancellable = true)
	public void renderGuiItem(ItemStack itemStack, int x, int y, IBakedModel p_191962_4_, CallbackInfo cir) {
		if (itemStack.getItem() instanceof Shuriken) {
			CompoundNBT nbt = itemStack.getTag();
			if (nbt != null) {
				Integer count = Shuriken.stackSize;
				if (nbt.contains(Shuriken.shurikenCount)) {
					count = nbt.getInt(Shuriken.shurikenCount);
				}
				if (count == Shuriken.stackSize && itemStack.getDamageValue() == 0) {
					return;
				}
				FontRenderer fontRenderer = mc.font;
				MatrixStack matrixstack = new MatrixStack();
				String s = count.toString();
				matrixstack.translate(0.0D, 0.0D, 380.0D);
				IRenderTypeBuffer.Impl irendertypebuffer$impl = IRenderTypeBuffer.immediate(Tessellator.getInstance().getBuilder());
				fontRenderer.drawInBatch(s, (float) (x + 17 - fontRenderer.width(s)), y, 16777215, true, matrixstack.last().pose(), irendertypebuffer$impl, false, 0, 15728880);
				irendertypebuffer$impl.endBatch();
			}
		}
	}
}