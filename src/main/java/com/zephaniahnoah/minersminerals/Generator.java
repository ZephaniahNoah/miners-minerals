package com.zephaniahnoah.minersminerals;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(modid = Main.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Generator {
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event) {
		if (event.includeClient())
			registerClientProviders(event.getGenerator(), event);
	}

	private static void registerClientProviders(DataGenerator generator, GatherDataEvent event) {
		// generator.addProvider(new GeneratorLanguage(generator));
		ExistingFileHelper helper = event.getExistingFileHelper();
		// generator.addProvider(new GeneratorBlockStates(generator, helper));
	}
}
