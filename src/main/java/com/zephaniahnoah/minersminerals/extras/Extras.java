package com.zephaniahnoah.minersminerals.extras;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import org.apache.commons.io.IOUtils;

import com.zephaniahnoah.ezmodlib.EzModLib;
import com.zephaniahnoah.ezmodlib.TagInjector;
import com.zephaniahnoah.ezmodlib.recipe.FurnaceRecipe;
import com.zephaniahnoah.ezmodlib.recipe.ShapedRecipe;
import com.zephaniahnoah.ezmodlib.recipe.SingleInputRecipe.SingleInputType;
import com.zephaniahnoah.ezmodlib.recipe.StoneCutterRecipe;
import com.zephaniahnoah.minersminerals.IMineralOre;
import com.zephaniahnoah.minersminerals.Main;
import com.zephaniahnoah.minersminerals.Mineral;
import com.zephaniahnoah.minersminerals.extras.clam.Clam;
import com.zephaniahnoah.minersminerals.extras.clam.ClamTileEntity;
import com.zephaniahnoah.minersminerals.extras.trex.TRexSkull;
import com.zephaniahnoah.minersminerals.extras.trex.TRexSkullItem;
import com.zephaniahnoah.minersminerals.extras.trex.TRexSkullTileEntity;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.Item.Properties;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTier;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome.Category;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;

// TODO: Stairs, slabs, and walls
// TODO: Petrified door and fences
// TODO: Armor upgrades
public class Extras {

	public static final List<Enchantment> skullEnchants = (List<Enchantment>) Arrays.asList(new Enchantment[] { Enchantments.PROJECTILE_PROTECTION, Enchantments.ALL_DAMAGE_PROTECTION, Enchantments.BLAST_PROTECTION, Enchantments.FIRE_PROTECTION, Enchantments.RESPIRATION, Enchantments.AQUA_AFFINITY, Enchantments.BINDING_CURSE, Enchantments.VANISHING_CURSE });
	public static List<Block> transparentBlocks = new ArrayList<Block>();
	public static Block fossil;
	public static RegistryObject<TileEntityType<?>> trexTileEntity;
	public static Block trexSkull;
	public static Block clam;
	public static Item trexSkullDummy;
	public static RegistryObject<TileEntityType<?>> clamTileEntity;
	public static final String starfishShuriken = "starfish_shuriken";

	public static final ItemGroup creativeTab = new ItemGroup(Main.MODID + ".extras") {
		@Override
		public ItemStack makeIcon() {
			return new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(Main.MODID + ":ammolite")));
		}
	};

	private static final String[] blocks = new String[] { //
			"blood_stone_bricks", //
			"blood_stone_tiles", //
			"cracked_blood_stone_bricks", //
			"jade_bricks", //
			"jade_tiles", //
			"jet_bricks", //
			"cracked_jet_bricks", //
			"jet_tile", //
			"polished_jet", //
			"bronzite_tile", //
			"bronzite_bricks", //
			"cracked_bronzite_bricks", //
			"polished_bronzite", //
			"charoite_tile", //
			"cracked_charoite_bricks", //
			"charoite_bricks", //
			"polished_charoite", //
			"chiseled_charoite", //
			"brown_calcite", //
			"steel_bricks", //
			"moss_agate_bricks", //
			"cut_osmium", //
			"osmium_bricks", //
			"osmium_tiles", //
			"deuterium_bricks", //
			"cut_deuterium", //
			"cut_copper", //
			"petrified_wood_planks", //
			"polished_blood_stone", //
			"polished_jade", //
			"chiseled_serpentine", //
			"polished_serpentine", //
			"serpentine_bricks", //
			"cracked_serpentine_bricks", //
			"serpentine_tile", //
			"polished_rhodonite", //
			"rhodonite_bricks", //
			"cracked_rhodonite_bricks", //
			"rhodonite_tile", //
			"chiseled_rhodonite" };

	private static final String[] items = new String[] { //
			"ammolite", //
			"dragon_scale" };

	public enum ExtraMineral implements IMineralOre {
		FOSSIL_DEPOSIT(new Category[] { Category.MESA });

		private Category[] biomes;

		ExtraMineral(Category[] biomes) {
			this.biomes = biomes;
		}

		@Override
		public Category[] biomes() {
			return biomes;
		}

		@Override
		public String getLowerName() {
			return this.name().toLowerCase();
		}
	}

	public static void init() {
		Block b;
		Item i;
		String n;

		for (String name : blocks) {
			EzModLib.register(Main.MODID, name, b = new Block(AbstractBlock.Properties.of(Material.STONE).requiresCorrectToolForDrops().strength(1.5F, 6.0F)), null, new BlockItem(b, new Item.Properties().tab(creativeTab)), null);
			Main.defaultBlockDrop(name);
		}

		for (String item : items) {
			EzModLib.register(Main.MODID, item, new Item(new Item.Properties().tab(creativeTab)));
		}

		cutStone("copper_block", "cut_copper");
		cutStone("moss_agate_block", "moss_agate_bricks");
		cutStone("steel_block", "steel_bricks");
		new StoneCutterRecipe(new ResourceLocation("petrified_wood_planks_recipe"), Main.MODID + ":petrified_log", Main.MODID + ":petrified_wood_planks", 4);

		cutStone("deuterium_block", "deuterium_bricks");
		cutStone("deuterium_block", "cut_deuterium");

		cutStone("osmium_block", "osmium_tiles");
		cutStone("osmium_block", "osmium_bricks");
		cutStone("osmium_block", "cut_osmium");

		cutStone("bronzite_block", "cracked_bronzite_bricks");
		cutStone("bronzite_block", "chiseled_bronzite");
		cutStone("bronzite_block", "polished_bronzite");
		cutStone("bronzite_block", "bronzite_bricks");
		cutStone("bronzite_block", "bronzite_tile");

		cutStone("charoite_block", "cracked_charoite_bricks");
		cutStone("charoite_block", "chiseled_charoite");
		cutStone("charoite_block", "polished_charoite");
		cutStone("charoite_block", "charoite_bricks");
		cutStone("charoite_block", "charoite_tile");

		cutStone("rhodonite_block", "cracked_rhodonite_bricks");
		cutStone("rhodonite_block", "rhodonite_tile");
		cutStone("rhodonite_block", "rhodonite_bricks");
		cutStone("rhodonite_block", "polished_rhodonite");
		cutStone("rhodonite_block", "chiseled_rhodonite");

		cutStone("jet_block", "cracked_jet_bricks");
		cutStone("jet_block", "polished_jet");
		cutStone("jet_block", "jet_tile");
		cutStone("jet_block", "jet_bricks");
		cutStone("jet_block", "chiseled_jet");

		cutStone("jade_block", "polished_jade");
		cutStone("jade_block", "jade_tiles");
		cutStone("jade_block", "jade_bricks");
		cutStone("jade_block", "chiseled_jade");

		cutStone("blood_stone_block", "cracked_blood_stone_bricks");
		cutStone("blood_stone_block", "polished_blood_stone");
		cutStone("blood_stone_block", "chiseled_blood_stone");
		cutStone("blood_stone_block", "blood_stone_tiles");
		cutStone("blood_stone_block", "blood_stone_bricks");

		cutStone("serpentine_block", "chiseled_serpentine");
		cutStone("serpentine_block", "polished_serpentine");
		cutStone("serpentine_block", "serpentine_bricks");
		cutStone("serpentine_block", "serpentine_tile");
		cutStone("serpentine_block", "serpentine_pillar");

		EzModLib.register(Main.MODID, "script_opal", i = new Item(new Item.Properties().tab(creativeTab)));
		TagInjector.items.inject("forge:gems/opal", i);

		EzModLib.register(Main.MODID, "mosquito_in_amber", i = new Item(new Item.Properties().tab(creativeTab)));
		TagInjector.items.inject("forge:gems/amber", i);

		EzModLib.register(Main.MODID, "black_diamond", i = new Item(new Item.Properties().tab(creativeTab)));
		TagInjector.items.inject("forge:gems/diamond", i);

		EzModLib.register(Main.MODID, n = "flower_fossil", add(b = new FlowerFossil(AbstractBlock.Properties.of(Material.STONE).noOcclusion().requiresCorrectToolForDrops().strength(1.5F, 3.0F))), EzModLib.INJECT_EXISTING_MODEL, new BlockItem(b, new Item.Properties().tab(creativeTab)), null);
		Main.defaultBlockDrop(n);
		EzModLib.register(Main.MODID, n = "fish_fossil", add(b = new FishFossil(AbstractBlock.Properties.of(Material.STONE).noOcclusion().requiresCorrectToolForDrops().strength(1.5F, 3.0F))), EzModLib.INJECT_EXISTING_MODEL, new BlockItem(b, new Item.Properties().tab(creativeTab)), null);
		Main.defaultBlockDrop(n);

		// Using a dummy item to hold the trex skull model
		// Should probably do this better
		EzModLib.register(Main.MODID, "trex_skull_fossil_dummy", trexSkullDummy = new Item(new Item.Properties()), EzModLib.DONT_INJECT);

		EzModLib.register(Main.MODID, n = "clam_fossil", add(clam = new Clam(AbstractBlock.Properties.of(Material.STONE).noOcclusion().strength(1.5F, 3))), EzModLib.DONT_INJECT, new BlockItem(clam, new Item.Properties().tab(creativeTab)), null);
		Main.defaultBlockDrop(n);

		Properties properties = new Item.Properties().tab(creativeTab).stacksTo(1);
		try {
			properties.setISTER(getRenderer());
		} catch (NoSuchMethodError ex) {
		}

		EzModLib.register(Main.MODID, "trex_skull_fossil", add(trexSkull = new TRexSkull(AbstractBlock.Properties.of(Material.STONE).noOcclusion().strength(1.5F, 6F))), EzModLib.DONT_INJECT, new TRexSkullItem(trexSkull, properties), EzModLib.DONT_INJECT);

		clamTileEntity = EzModLib.register(Main.MODID, "clam_fossil", ClamTileEntity::new, clam);
		trexTileEntity = EzModLib.register(Main.MODID, "trex_skull_fossil", TRexSkullTileEntity::new, trexSkull);

		EzModLib.register(Main.MODID, starfishShuriken, b = new BasicFossil(AbstractBlock.Properties.of(Material.STONE).noOcclusion()) {
			@Override
			public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
				return box(13, 0, 13, 3, 2, 3);
			}

			@Override
			public void entityInside(BlockState blockState, World world, BlockPos blockPos, Entity entity) {
				if (!(entity instanceof ItemEntity)) {
					entity.hurt(DamageSource.CACTUS, 3F);
				}
			}
		}, EzModLib.DONT_INJECT, i = new StarfishItem(ItemTier.WOOD, new Item.Properties().tab(creativeTab), add(b)), "{\"parent\":\"minecraft:item/generated\",\"textures\":{\"layer0\":\"" + Main.MODID + ":item/" + starfishShuriken + "\"}}");
		Main.defaultBlockDrop(starfishShuriken);
		Main.shurikens.put(starfishShuriken, i);

		EzModLib.register(Main.MODID, n = "fossil_deposit_block", fossil = new Block(AbstractBlock.Properties.of(Material.STONE).requiresCorrectToolForDrops().strength(1.5F, 5F)), EzModLib.DONT_INJECT, new BlockItem(fossil, new Item.Properties().tab(creativeTab)), null);

		reg("petrified_log", 2F, 7F);
		reg("chiseled_bronzite", 1.5F, 6F);
		reg("chiseled_blood_stone", 1.5F, 6F);
		reg("chiseled_jade", 1.5F, 6F);
		reg("serpentine_pillar", 1.5F, 6F);
		reg("chiseled_jet", 1.5F, 6F);

		new ShapedRecipe(new ResourceLocation("amber_torch"), new String[] { "#", "/" }, Items.TORCH, 8).tag('#', Mineral.AMBER.tag).tag('/', "forge:rods/wooden");
	}

	private static void reg(String name, float destroyTime, float blastResistance) {
		Block b;
		EzModLib.register(Main.MODID, name, b = new RotatedPillarBlock(AbstractBlock.Properties.of(Material.STONE).requiresCorrectToolForDrops().strength(destroyTime, blastResistance)), pillarModel(name), new BlockItem(b, new Item.Properties().tab(creativeTab)), null);
		Main.defaultBlockDrop(name);
	}

	@OnlyIn(Dist.CLIENT)
	private static Supplier<Callable<ItemStackTileEntityRenderer>> getRenderer() {
		return () -> () -> com.zephaniahnoah.minersminerals.extras.trex.TRexSkullItemRenderer.INSTANCE;
	}

	private static void cutStone(String ingredient, String result) {
		new StoneCutterRecipe(new ResourceLocation(result + "_recipe"), Main.MODID + ":" + ingredient, Main.MODID + ":" + result, 1);
		new FurnaceRecipe(new ResourceLocation(result + "_smelt"), SingleInputType.SMELTING, Main.MODID + ":" + result, Main.MODID + ":" + ingredient, 200, 0.7);
	}

	private static Block add(Block block) {
		transparentBlocks.add(block);
		return block;
	}

	private static String pillarModel(String file) {
		try (InputStream in = Extras.class.getResourceAsStream("/assets/" + Main.MODID + "/models/block/" + file + "_horizontal.json")) {
			return IOUtils.toString(in, "UTF-8").replaceAll("minecraft:block/cube_column_horizontal", "minecraft:block/cube_column");
		} catch (Exception e) {
			System.out.println("Failed to load " + file);
			e.printStackTrace();
			return null;
		}
	}
}
