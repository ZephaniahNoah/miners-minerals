package com.zephaniahnoah.minersminerals.extras.trex;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.vector.Vector3f;

public class TRexSkullItemRenderer extends ItemStackTileEntityRenderer {

	public static final TRexSkullItemRenderer INSTANCE = new TRexSkullItemRenderer();
	private static TRexSkullTileEntity virtualTileEntity;

	@Override
	public void renderByItem(ItemStack itemStack, ItemCameraTransforms.TransformType transformType, MatrixStack matrixStack, IRenderTypeBuffer buffer, int p_239207_5_, int p_239207_6_) {
		getTileEntity().itemStack.setTag(itemStack.getTag());

		// Maybe there is a way to retrieve these from the dummy model
		double c = .35;
		switch (transformType) {
		case NONE:
			// matrixStack.translate(.01, .01, -.01);
			// matrixStack.scale(.999f, .999f, .999f);
			break;
		case THIRD_PERSON_LEFT_HAND:
			matrixStack.translate(c, .1, .2);
			matrixStack.scale(.3f, .3f, .3f);
			break;
		case THIRD_PERSON_RIGHT_HAND:
			matrixStack.translate(c, .1, .2);
			matrixStack.scale(.3f, .3f, .3f);
			break;
		case FIRST_PERSON_LEFT_HAND:
			matrixStack.translate(c, .2, .2);
			matrixStack.scale(.3f, .3f, .3f);
			break;
		case FIRST_PERSON_RIGHT_HAND:
			matrixStack.translate(c, .2, .2);
			matrixStack.scale(.3f, .3f, .3f);
			break;
		case HEAD:
			matrixStack.translate(.1, -.15, -.45);
			matrixStack.scale(.8f, .8f, .8f);
			break;
		case GUI:
			matrixStack.translate(.42, .02, 0);
			matrixStack.scale(.44f, .44f, .44f);
			matrixStack.mulPose(Vector3f.XN.rotationDegrees(-20.0F));
			matrixStack.mulPose(Vector3f.YN.rotationDegrees(-140.0F));
			break;
		case GROUND:
			matrixStack.translate(.3, .3, .2);
			matrixStack.scale(.4f, .4f, .4f);
			break;
		case FIXED:
			matrixStack.translate(.25, .1, -.1);
			matrixStack.scale(.5f, .5f, .5f);
			break;
		}

		TileEntityRendererDispatcher.instance.renderItem(getTileEntity(), matrixStack, buffer, p_239207_5_, p_239207_6_);
	}

	private TRexSkullTileEntity getTileEntity() {
		if (virtualTileEntity == null) {
			virtualTileEntity = new TRexSkullTileEntity();
			virtualTileEntity.data = new CompoundNBT();
			virtualTileEntity.data.put("TrexSkullData", new CompoundNBT());
		}
		return virtualTileEntity;
	}
}