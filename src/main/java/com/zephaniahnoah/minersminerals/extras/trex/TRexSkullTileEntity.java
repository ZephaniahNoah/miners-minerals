package com.zephaniahnoah.minersminerals.extras.trex;

import com.zephaniahnoah.minersminerals.extras.Extras;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class TRexSkullTileEntity extends TileEntity {

	public CompoundNBT data;
	public ItemStack itemStack = ItemStack.EMPTY;

	public TRexSkullTileEntity(TileEntityType<?> type) {
		super(type);
	}

	public TRexSkullTileEntity() {
		this(Extras.trexTileEntity.get());
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt) {
		super.save(nbt);
		if (this.data != null) {
			nbt.put("TrexSkullData", this.data);
		}

		return nbt;
	}

	@Override
	public void load(BlockState b, CompoundNBT nbt) {
		super.load(b, nbt);
		this.data = nbt.getCompound("TrexSkullData");
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.worldPosition, -1, getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.save(new CompoundNBT());
	}

	@Override
	public void handleUpdateTag(BlockState blockState, CompoundNBT parentNBTTagCompound) {
		this.load(blockState, parentNBTTagCompound);
	}

	@Override
	public void setRemoved() {
		super.setRemoved();
		ItemStack stack = new ItemStack(Extras.trexSkull);
		stack.setTag(getItemStackFromData().getTag());
		Block.popResource(this.level, this.worldPosition, stack);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.load(this.level.getBlockState(pkt.getPos()), pkt.getTag());
	}

	public ItemStack getItemStackFromData() {
		if (itemStack == ItemStack.EMPTY) {
			ItemStack stack = new ItemStack(Extras.trexSkullDummy);
			if (data != null) {
				itemStack = stack;
				itemStack.setTag(data);
			}
		}
		return itemStack;
	}
}