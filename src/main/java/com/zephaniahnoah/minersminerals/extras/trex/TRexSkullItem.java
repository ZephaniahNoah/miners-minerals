package com.zephaniahnoah.minersminerals.extras.trex;

import java.util.UUID;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.zephaniahnoah.minersminerals.extras.Extras;
import com.google.common.collect.ImmutableMultimap.Builder;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.IArmorVanishable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.monster.EndermanEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class TRexSkullItem extends BlockItem {

	private final Multimap<Attribute, AttributeModifier> defaultModifiers;
	private EquipmentSlotType slot;

	public TRexSkullItem(Block block, Properties properties) {
		super(block, properties);
		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		UUID[] ARMOR_MODIFIER_UUID_PER_SLOT = ObfuscationReflectionHelper.getPrivateValue(ArmorItem.class, null, "field_185084_n");// ARMOR_MODIFIER_UUID_PER_SLOT
		slot = EquipmentSlotType.HEAD;
		UUID uuid = ARMOR_MODIFIER_UUID_PER_SLOT[slot.getIndex()];
		builder.put(Attributes.ARMOR, new AttributeModifier(uuid, "Armor modifier", 6, AttributeModifier.Operation.ADDITION));
		this.defaultModifiers = builder.build();
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slotType) {
		return slotType == this.slot ? this.defaultModifiers : super.getDefaultAttributeModifiers(slotType);
	}

	@Override
	public ActionResultType place(BlockItemUseContext context) {
		ItemStack itemStack = context.getItemInHand();
		if (itemStack.getTag() == null)
			itemStack.setTag(new CompoundNBT());
		CompoundNBT tag = itemStack.getTag();
		CompoundNBT compoundnbt = itemStack.getTagElement("BlockEntityTag");

		if (compoundnbt != null) {
			tag.remove("BlockEntityTag");
		}

		CompoundNBT skullData = new CompoundNBT();

		skullData.put("TrexSkullData", tag.copy());
		tag.put("BlockEntityTag", skullData);

		return super.place(context);
	}

	@Override
	public boolean canEquip(ItemStack stack, EquipmentSlotType armorType, Entity entity) {
		return armorType == EquipmentSlotType.HEAD;
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment ench) {
		return Extras.skullEnchants.contains(ench);
	}

	@Override
	public boolean isEnderMask(ItemStack stack, PlayerEntity player, EndermanEntity endermanEntity) {
		return true;
	}

	@Override
	public boolean isEnchantable(ItemStack itemStack) {
		return true;
	}

	@Override
	public int getEnchantmentValue() {
		return 5;
	}
}