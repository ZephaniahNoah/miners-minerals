package com.zephaniahnoah.minersminerals.extras.trex;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.vertex.VertexBuilderUtils;
import com.zephaniahnoah.minersminerals.extras.Extras;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Atlases;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.container.PlayerContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3f;

public class TRexSkullRenderer extends TileEntityRenderer<TRexSkullTileEntity> {

	private final ItemStack empty;

	public TRexSkullRenderer(TileEntityRendererDispatcher renderDispatcher) {
		super(renderDispatcher);
		empty = new ItemStack(Extras.trexSkullDummy);
	}

	@Override
	public void render(TRexSkullTileEntity tileEntity, float p_225616_2_, MatrixStack matrixStack, IRenderTypeBuffer renderTypeBuffer, int combinedLightIn, int combinedOverlayIn) {
		Direction direction;
		if (tileEntity.hasLevel()) {
			direction = tileEntity.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING);
		} else {
			direction = Direction.NORTH;
		}
		ItemStack itemstack = tileEntity.getItemStackFromData();
		if (itemstack == ItemStack.EMPTY || itemstack == null) {
			itemstack = empty;
		}

		matrixStack.pushPose();
		matrixStack.translate(0.5D, 0.5, 0.5D);
		Direction direction1 = Direction.from2DDataValue((direction.get2DDataValue()) % 4);
		float f = -direction1.toYRot();
		matrixStack.mulPose(Vector3f.YP.rotationDegrees(f));
		matrixStack.translate(0, -.2343, -.1565);
		matrixStack.scale(5, 5, 5);
		matrixStack.mulPose(Vector3f.YP.rotationDegrees(180.0F));
		IBakedModel ibakedmodel = this.itemRenderer().getModel(itemstack, tileEntity.getLevel(), (LivingEntity) null);

		matrixStack.pushPose();
		ibakedmodel = net.minecraftforge.client.ForgeHooksClient.handleCameraTransforms(matrixStack, ibakedmodel, ItemCameraTransforms.TransformType.GROUND, false);
		matrixStack.translate(-0.5D, -0.5D, -0.5D);
		RenderType renderType = RenderType.entityCutoutNoCull(PlayerContainer.BLOCK_ATLAS);
		IVertexBuilder ivertexbuilder = itemstack.hasFoil() ? VertexBuilderUtils.create(renderTypeBuffer.getBuffer(RenderType.glintDirect()), renderTypeBuffer.getBuffer(renderType)) : renderTypeBuffer.getBuffer(renderType);
		itemRenderer().renderModelLists(ibakedmodel, itemstack, combinedLightIn, OverlayTexture.NO_OVERLAY, matrixStack, ivertexbuilder);
		matrixStack.popPose();
		matrixStack.popPose();

		// This calls IRenderTypeBuffer.Impl.getBuffer()
		// Not going to pretend that I know how it works. But I know what it does. It fixes the missing enchant glint and that's all that I care about.
		renderTypeBuffer.getBuffer(Atlases.translucentCullBlockSheet());
	}

	public static IVertexBuilder getFoilBufferDirect(IRenderTypeBuffer renderTypeBuffer, RenderType renderType, boolean bool, boolean enchanted) {
		return null;
	}

	private ItemRenderer itemRenderer() {
		return Minecraft.getInstance().getItemRenderer();
	}
}
