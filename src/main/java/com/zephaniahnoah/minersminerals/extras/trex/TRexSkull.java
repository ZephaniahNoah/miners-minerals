package com.zephaniahnoah.minersminerals.extras.trex;

import com.mojang.datafixers.util.Pair;
import com.zephaniahnoah.minersminerals.extras.Extras;
import com.zephaniahnoah.minersminerals.extras.HorizontalFossilBlock;

import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;

public class TRexSkull extends HorizontalFossilBlock {

	private static final Pair<Integer, Integer> sideA = new Pair<Integer, Integer>(2, 8);
	private static final Pair<Integer, Integer> sideB = new Pair<Integer, Integer>(-2, -16);
	private static final Pair<Integer, Integer> topA = new Pair<Integer, Integer>(0, 8);
	private static final Pair<Integer, Integer> topB = new Pair<Integer, Integer>(-16, -19);
	private static final Pair<Integer, Integer> noseA = new Pair<Integer, Integer>(0, -18);
	private static final Pair<Integer, Integer> noseB = new Pair<Integer, Integer>(-16, -24);

	private static final Pair<Integer, Integer> movementBase = new Pair<Integer, Integer>(-1, 0);

	private final VoxelShape N;
	private final VoxelShape S;
	private final VoxelShape E;
	private final VoxelShape W;

	public TRexSkull(Properties properties) {
		super(properties, VoxelShapes.block(), VoxelShapes.block());
		N = getMirroredModel(true, false);
		S = getMirroredModel(false, false);
		E = getMirroredModel(false, true);
		W = getMirroredModel(true, true);
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		switch (state.getValue(FACING)) {
		case NORTH:
			return N;
		case SOUTH:
			return S;
		case EAST:
			return E;
		case WEST:
			return W;
		default:
			return VoxelShapes.block();
		}
	}

	private static VoxelShape getMirroredModel(boolean mirrorX, boolean mirrorZ) {
		int m = mirrorX ? 1 : -1;
		Pair<Integer, Integer> A = mirrorZ ? sideA.swap() : sideA;
		Pair<Integer, Integer> B = mirrorZ ? sideB.swap() : sideB;
		VoxelShape side = box(A.getFirst() * m, 0, A.getSecond() * m, B.getFirst() * m, 24, B.getSecond() * m);
		Pair<Integer, Integer> A2 = mirrorZ ? topA.swap() : topA;
		Pair<Integer, Integer> B2 = mirrorZ ? topB.swap() : topB;
		Pair<Integer, Integer> C = mirrorZ ? noseA.swap() : noseA;
		Pair<Integer, Integer> D = mirrorZ ? noseB.swap() : noseB;
		Pair<Integer, Integer> base = mirrorZ ? movementBase.swap() : movementBase;
		VoxelShape whole = VoxelShapes.or(side, side.move(base.getFirst() * m, 0, base.getSecond() * m), box(A2.getFirst() * m, 24, A2.getSecond() * m, B2.getFirst() * m, 28, B2.getSecond() * m), box(C.getFirst() * m, 14, C.getSecond() * m, D.getFirst() * m, 24, D.getSecond() * m));
		return whole.move(mirrorX ? 1 : 0, 0, mirrorX ? 1 : 0);
	}

	@Override
	public boolean hasTileEntity(BlockState b) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState b, IBlockReader world) {
		return Extras.trexTileEntity.get().create();
	}
}
