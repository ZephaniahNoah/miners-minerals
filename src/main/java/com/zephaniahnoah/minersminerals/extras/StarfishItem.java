package com.zephaniahnoah.minersminerals.extras;

import java.util.List;

import javax.annotation.Nullable;

import com.zephaniahnoah.minersminerals.extras.shuriken.Shuriken;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.Property;
import net.minecraft.state.StateContainer;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class StarfishItem extends Shuriken {

	private Block block;

	public StarfishItem(IItemTier mineral, Properties properties, Block block) {
		super(mineral, properties);
		this.block = block;
	}

	@Override
	public ActionResultType useOn(ItemUseContext useContext) {
		ActionResultType actionresulttype = this.place(new BlockItemUseContext(useContext));

		if (!actionresulttype.consumesAction() && this.isEdible()) {
			actionresulttype = this.use(useContext.getLevel(), useContext.getPlayer(), useContext.getHand()).getResult();
		}

		return actionresulttype;
	}

	public ActionResultType place(BlockItemUseContext blockitemusecontext) {
		if (!blockitemusecontext.canPlace()) {
			return ActionResultType.FAIL;
		} else {
			BlockState blockstate = this.getPlacementState(blockitemusecontext);
			if (blockstate == null) {
				return ActionResultType.FAIL;
			} else if (!this.placeBlock(blockitemusecontext, blockstate)) {
				return ActionResultType.FAIL;
			} else {
				BlockPos blockpos = blockitemusecontext.getClickedPos();
				World world = blockitemusecontext.getLevel();
				PlayerEntity playerentity = blockitemusecontext.getPlayer();
				ItemStack itemstack = blockitemusecontext.getItemInHand();
				BlockState blockstate1 = world.getBlockState(blockpos);
				Block block = blockstate1.getBlock();
				if (block == blockstate.getBlock()) {
					blockstate1 = this.updateBlockStateFromTag(blockpos, world, itemstack, blockstate1);
					// this.updateCustomBlockEntityTag(blockpos, world, playerentity, itemstack, blockstate1);
					block.setPlacedBy(world, blockpos, blockstate1, playerentity, itemstack);
					if (playerentity instanceof ServerPlayerEntity) {
						CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) playerentity, blockpos, itemstack);
					}
				}

				SoundType soundtype = blockstate1.getSoundType(world, blockpos, blockitemusecontext.getPlayer());
				world.playSound(playerentity, blockpos, this.getPlaceSound(blockstate1, world, blockpos, blockitemusecontext.getPlayer()), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0F) / 2.0F, soundtype.getPitch() * 0.8F);
				if (playerentity == null || !playerentity.abilities.instabuild) {
					itemstack.shrink(1);
				}

				return ActionResultType.sidedSuccess(world.isClientSide);
			}
		}
	}

	protected SoundEvent getPlaceSound(BlockState state, World world, BlockPos pos, PlayerEntity entity) {
		return state.getSoundType(world, pos, entity).getPlaceSound();
	}

	@Override
	public boolean isDamageable(ItemStack stack) {
		return false;
	}

	@Override
	public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
		ItemStack itemstack = player.getItemInHand(hand);
		player.startUsingItem(hand);
		return ActionResult.consume(itemstack);
	}

	@Override
	public int getItemStackLimit(ItemStack stack) {
		return 1;
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment ench) {
		return false;
	}

	@Nullable
	protected BlockState getPlacementState(BlockItemUseContext useContext) {
		BlockState blockstate = this.getBlock().getStateForPlacement(useContext);
		return blockstate != null && this.canPlace(useContext, blockstate) ? blockstate : null;
	}

	private BlockState updateBlockStateFromTag(BlockPos blockPos, World world, ItemStack itemStack, BlockState blockState) {
		BlockState blockstate = blockState;
		CompoundNBT compoundnbt = itemStack.getTag();
		if (compoundnbt != null) {
			CompoundNBT compoundnbt1 = compoundnbt.getCompound("BlockStateTag");
			StateContainer<Block, BlockState> statecontainer = blockState.getBlock().getStateDefinition();

			for (String s : compoundnbt1.getAllKeys()) {
				Property<?> property = statecontainer.getProperty(s);
				if (property != null) {
					String s1 = compoundnbt1.get(s).getAsString();
					blockstate = updateState(blockstate, property, s1);
				}
			}
		}

		if (blockstate != blockState) {
			world.setBlock(blockPos, blockstate, 2);
		}

		return blockstate;
	}

	private <T extends Comparable<T>> BlockState updateState(BlockState blockState, Property<T> property, String string) {
		return property.getValue(string).map((p_219986_2_) -> {
			return blockState.setValue(property, p_219986_2_);
		}).orElse(blockState);
	}

	protected boolean canPlace(BlockItemUseContext context, BlockState blockState) {
		PlayerEntity playerentity = context.getPlayer();
		ISelectionContext iselectioncontext = playerentity == null ? ISelectionContext.empty() : ISelectionContext.of(playerentity);
		return (!this.mustSurvive() || blockState.canSurvive(context.getLevel(), context.getClickedPos())) && context.getLevel().isUnobstructed(blockState, context.getClickedPos(), iselectioncontext);
	}

	protected boolean mustSurvive() {
		return true;
	}

	protected boolean placeBlock(BlockItemUseContext context, BlockState blockState) {
		return context.getLevel().setBlock(context.getClickedPos(), blockState, 11);
	}

	@Override
	public String getDescriptionId() {
		return this.getBlock().getDescriptionId();
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack itemStack, @Nullable World world, List<ITextComponent> textComponents, ITooltipFlag tooltipFlag) {
		super.appendHoverText(itemStack, world, textComponents, tooltipFlag);
		this.getBlock().appendHoverText(itemStack, world, textComponents, tooltipFlag);
	}

	public Block getBlock() {
		return this.getBlockRaw() == null ? null : this.getBlockRaw().delegate.get();
	}

	private Block getBlockRaw() {
		return block;
	}
}
