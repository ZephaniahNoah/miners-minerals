package com.zephaniahnoah.minersminerals.extras.shuriken;

import com.zephaniahnoah.ezmodlib.EzModLib;
import com.zephaniahnoah.minersminerals.Main;
import com.zephaniahnoah.minersminerals.extras.Extras;
import com.zephaniahnoah.minersminerals.extras.spear.SpearEntity;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.network.NetworkHooks;

public class ShurikenEntity extends AbstractArrowEntity {

	private static final String defaultShuriken = "copper_shuriken";
	private static final DataParameter<Byte> ID_LOYALTY = ObfuscationReflectionHelper.getPrivateValue(TridentEntity.class, null, "field_203053_g");// ID_LOYALTY
	private static final DataParameter<Boolean> ID_FOIL = ObfuscationReflectionHelper.getPrivateValue(TridentEntity.class, null, "field_226571_aq_");// ID_FOIL
	private static final DataParameter<String> ID_OWNER = EntityDataManager.defineId(SpearEntity.class, DataSerializers.STRING);
	private static final DataParameter<String> ITEM = EntityDataManager.defineId(SpearEntity.class, DataSerializers.STRING);
	public static RegistryObject shurikenType;
	private int life;
	private ItemStack tridentItem = new ItemStack(Items.TRIDENT);
	private boolean dealtDamage;
	public int clientSideReturnTridentTickCount;
	private String cacaheNameLookup = null;

	public ShurikenEntity(EntityType<?> shuriken, World world) {
		super((EntityType<? extends ShurikenEntity>) shuriken, world);
	}

	public ShurikenEntity(World world, LivingEntity thrower, ItemStack shuriken) {
		super((EntityType) shurikenType.get(), world);
		this.setOwner(thrower);
		this.setPos(thrower.getX(), thrower.getEyeY() - (double) 0.1F, thrower.getZ());
		tridentItem = shuriken;
		this.entityData.set(ID_LOYALTY, (byte) EnchantmentHelper.getLoyalty(shuriken));
		this.entityData.set(ID_FOIL, shuriken.hasFoil());
		this.entityData.set(ID_OWNER, thrower instanceof PlayerEntity ? ((PlayerEntity) thrower).getName().getContents() : "");
		this.entityData.set(ITEM, shurikenName(shuriken));
	}

	@OnlyIn(Dist.CLIENT)
	public ShurikenEntity(World world_, double x, double y, double z) {
		super(EntityType.TRIDENT, x, y, z, world_);
	}

	private String shurikenName(ItemStack shuriken) {
		return shuriken.getItem().getRegistryName().getPath();
	}

	@Override
	protected void defineSynchedData() {
		super.defineSynchedData();
		this.entityData.define(ITEM, defaultShuriken);
		this.entityData.define(ID_OWNER, "");
		this.entityData.define(ID_LOYALTY, (byte) 0);
		this.entityData.define(ID_FOIL, false);
	}

	public ItemStack getItemStack() {
		return new ItemStack(Main.shurikens.get(entityData.get(ITEM)));
	}

	protected ItemStack getPickupItem() {
		return this.tridentItem.copy();
	}

	@OnlyIn(Dist.CLIENT)
	public boolean isFoil() {
		return this.entityData.get(ID_FOIL);
	}

	protected SoundEvent getDefaultHitGroundSoundEvent() {
		return SoundEvents.TRIDENT_HIT_GROUND;
	}

	@Override
	protected void onHitEntity(EntityRayTraceResult result) {
		Entity entity = result.getEntity();
		ItemStack shuriken = getPickupItem();
		float f;
		if (shuriken.getItem() instanceof Shuriken) {
			Shuriken item = (Shuriken) shuriken.getItem();
			f = item.tier.getAttackDamageBonus();
		} else {
			f = 2.5F;
		}
		f += 1;

		if (entity instanceof LivingEntity) {
			LivingEntity livingentity = (LivingEntity) entity;
			f += EnchantmentHelper.getDamageBonus(shuriken, livingentity.getMobType());
		}

		boolean flag = entity.getType() == EntityType.ENDERMAN;
		int k = entity.getRemainingFireTicks();
		if (this.isOnFire() && !flag) {
			entity.setSecondsOnFire(5);
		}

		Entity entity1 = this.getOwner();
		DamageSource damagesource = DamageSource.trident(this, (Entity) (entity1 == null ? this : entity1));
		dealtDamage = true;
		SoundEvent soundevent = SoundEvents.TRIDENT_HIT;
		if (entity.hurt(damagesource, f)) {
			if (flag) {
				return;
			}

			int knockback = ObfuscationReflectionHelper.getPrivateValue(AbstractArrowEntity.class, this, "field_70256_ap");// knockback

			if (knockback > 0) {
				Vector3d vector3d = this.getDeltaMovement().multiply(1.0D, 0.0D, 1.0D).normalize().scale((double) knockback * 0.6D);
				if (vector3d.lengthSqr() > 0.0D) {
					entity.push(vector3d.x, 0.1D, vector3d.z);
				}
			}

			if (entity instanceof LivingEntity) {
				LivingEntity livingentity1 = (LivingEntity) entity;
				if (entity1 instanceof LivingEntity) {
					EnchantmentHelper.doPostHurtEffects(livingentity1, entity1);
					EnchantmentHelper.doPostDamageEffects((LivingEntity) entity1, livingentity1);
				}

				this.doPostHurtEffects(livingentity1);
			}
		} else {
			entity.setRemainingFireTicks(k);
		}

		this.setDeltaMovement(this.getDeltaMovement().multiply(-0.01D, -0.1D, -0.01D));
		float f1 = 1.0F;

		if (this.level instanceof ServerWorld && this.level.isThundering() && EnchantmentHelper.hasChanneling(shuriken)) {
			BlockPos blockpos = entity.blockPosition();
			if (this.level.canSeeSky(blockpos)) {
				LightningBoltEntity lightningboltentity = EntityType.LIGHTNING_BOLT.create(this.level);
				lightningboltentity.moveTo(Vector3d.atBottomCenterOf(blockpos));
				lightningboltentity.setCause(entity1 instanceof ServerPlayerEntity ? (ServerPlayerEntity) entity1 : null);
				this.level.addFreshEntity(lightningboltentity);
				soundevent = SoundEvents.TRIDENT_THUNDER;
				f1 = 5.0F;
			}
		}

		this.playSound(soundevent, f1, 1.0F);
	}

	@Override
	public void tick() {
		if (this.inGroundTime > 4) {
			dealtDamage = true;
		}

		Entity entity = this.getOwner();
		if ((dealtDamage || this.isNoPhysics()) && entity != null) {
			int i = this.entityData.get(ID_LOYALTY);
			if (i > 0 && !this.isAcceptibleReturnOwner()) {
				// if (!this.level.isClientSide && this.pickup == AbstractArrowEntity.PickupStatus.ALLOWED) {
				// this.spawnAtLocation(this.getPickupItem(), 0.1F);
				// }
				// this.remove();
			} else if (i > 0) {
				this.setNoPhysics(true);
				Vector3d vector3d = new Vector3d(entity.getX() - this.getX(), entity.getEyeY() - this.getY(), entity.getZ() - this.getZ());
				this.setPosRaw(this.getX(), this.getY() + vector3d.y * 0.015D * (double) i, this.getZ());
				if (this.level.isClientSide) {
					this.yOld = this.getY();
				}

				double d0 = 0.05D * (double) i;
				this.setDeltaMovement(this.getDeltaMovement().scale(0.95D).add(vector3d.normalize().scale(d0)));
				if (this.clientSideReturnTridentTickCount == 0) {
					this.playSound(SoundEvents.TRIDENT_RETURN, 10.0F, 1.0F);
				}

				++this.clientSideReturnTridentTickCount;
			}
		}

		super.tick();
	}

	private boolean isAcceptibleReturnOwner() {
		Entity entity = this.getOwner();
		if (entity != null && entity.isAlive()) {
			return !(entity instanceof ServerPlayerEntity) || !entity.isSpectator();
		} else {
			return false;
		}
	}

	@Override
	public void playerTouch(PlayerEntity player) {
		Entity entity = this.getOwner();
		if (entity == null || entity.getUUID() == player.getUUID()) {
			if (!this.level.isClientSide && (this.inGround || this.isNoPhysics()) && this.shakeTime <= 0) {
				if (player.abilities.instabuild) {
					player.take(this, 1);
					this.remove();
					return;
				}
				boolean pickupsAllowed = this.pickup == AbstractArrowEntity.PickupStatus.ALLOWED;
				if (pickupsAllowed || this.isNoPhysics() && this.getOwner().getUUID() == player.getUUID()) {
					pickup(player, false);
					player.take(this, 1);
					this.remove();
				}
			}
		}
	}

	private boolean pickup(PlayerEntity player, boolean defaultValue) {
		for (ItemStack i : player.inventory.items) {
			if (check(i, false)) {
				return true;
			}
		}
		return check(player.inventory.offhand.get(0), defaultValue);
	}

	private boolean check(ItemStack i, boolean defaultValue) {
		if (i.getItem() == this.getPickupItem().getItem()) {
			if (i.getEnchantmentTags().equals(this.getPickupItem().getEnchantmentTags())) {
				CompoundNBT tag = i.getTag();
				if (tag == null) {
					return false;
				}
				int count = Shuriken.stackSize;
				if (tag.contains(Shuriken.shurikenCount)) {
					count = tag.getInt(Shuriken.shurikenCount);
				}
				if (count < Shuriken.stackSize) {
					tag.putInt(Shuriken.shurikenCount, ++count);
					i.setTag(tag);
					return true;
				}
			}
		}
		return defaultValue;
	}

	@Override
	public void readAdditionalSaveData(CompoundNBT nbt) {
		super.readAdditionalSaveData(nbt);
		if (nbt.contains("Trident", 10)) {
			this.tridentItem = ItemStack.of(nbt.getCompound("Trident"));
		}

		this.dealtDamage = nbt.getBoolean("DealtDamage");
		this.entityData.set(ID_LOYALTY, (byte) EnchantmentHelper.getLoyalty(this.tridentItem));
		this.entityData.set(ITEM, nbt.getString("Shuriken"));
	}

	@Override
	public void addAdditionalSaveData(CompoundNBT nbt) {
		super.addAdditionalSaveData(nbt);
		nbt.put("Trident", this.tridentItem.save(new CompoundNBT()));
		nbt.putBoolean("DealtDamage", this.dealtDamage);
		nbt.putString("Shuriken", shurikenName(this.getPickupItem()));
	}

	@Override
	public IPacket<?> getAddEntityPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	protected float getWaterInertia() {
		if (cacaheNameLookup == null) {
			cacaheNameLookup = entityData.get(ITEM);
		}
		return cacaheNameLookup.equals(Shuriken.prismarine) || cacaheNameLookup.equals(Extras.starfishShuriken) ? 0.99F : super.getWaterInertia();
	}

	@Override
	public void tickDespawn() {
		int i = this.entityData.get(ID_LOYALTY);
		if (this.pickup != AbstractArrowEntity.PickupStatus.ALLOWED || i <= 0) {
			++this.life;
			if (this.life >= Shuriken.lifeTime) {
				String owner = this.entityData.get(ID_OWNER);
				for (PlayerEntity p : this.getCommandSenderWorld().players()) {
					if (p.getName().getContents().equals(owner)) {
						if (entityData.get(ITEM).equals(Extras.starfishShuriken)) {
							break;
						}
						if (pickup(p, false)) {
							break;
						}
					}
				}
				this.remove();
			}
		}
	}
}
