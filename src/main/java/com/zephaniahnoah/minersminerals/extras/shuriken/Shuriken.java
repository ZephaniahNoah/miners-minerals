package com.zephaniahnoah.minersminerals.extras.shuriken;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.stats.Stats;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

// TODO: Throw animation
// TODO: Should they break?
// TODO: Maybe add piercing??
// TODO: Randomize shuriken rotation (roll?)
public class Shuriken extends TridentItem {

	private static final List<Enchantment> enchants = (List<Enchantment>) Arrays.asList(new Enchantment[] { Enchantments.MULTISHOT, Enchantments.UNBREAKING, Enchantments.LOYALTY, Enchantments.MENDING, Enchantments.PUNCH_ARROWS, Enchantments.KNOCKBACK, Enchantments.FIRE_ASPECT, Enchantments.FLAMING_ARROWS });
	public static final String shurikenCount = "ShurikenCount";
	public final Multimap<Attribute, AttributeModifier> defaultModifiers;
	public final IItemTier tier;
	public static int ticks = 0;
	public static final String prismarine = "prismarine_shuriken";
	public static final int lifeTime = 600;
	public static final int stackSize = 16;

	public Shuriken(IItemTier mineral, Properties properties) {
		super(properties);
		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", (double) mineral.getAttackDamageBonus() / 1.5, AttributeModifier.Operation.ADDITION));
		builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Tool modifier", (double) -2.9F, AttributeModifier.Operation.ADDITION));
		this.defaultModifiers = builder.build();
		this.tier = mineral;
	}

	@Override
	public UseAction getUseAnimation(ItemStack itemStack) {
		return UseAction.NONE;
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slot) {
		return slot == EquipmentSlotType.MAINHAND ? this.defaultModifiers : super.getDefaultAttributeModifiers(slot);
	}

	@Override
	public void releaseUsing(ItemStack shuriken, World world, LivingEntity user, int timer) {
		if (user instanceof PlayerEntity) {
			PlayerEntity playerentity = (PlayerEntity) user;
			if (!world.isClientSide) {
				int count = stackSize;
				CompoundNBT nbt = shuriken.getTag();
				if (nbt != null) {
					if (nbt.contains(shurikenCount)) {
						count = nbt.getInt(shurikenCount);
					}
				}
				if (count > 0 || playerentity.abilities.instabuild) {
					shuriken.hurtAndBreak(1, playerentity, (player) -> {
						player.broadcastBreakEvent(user.getUsedItemHand());
					});
					int multishotLevel = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.MULTISHOT, shuriken);

					ShurikenEntity shurikenEntity = null;

					int length = multishotLevel == 0 ? 1 : (count > 3 ? 3 : count);

					for (int i = 0; i < length; i++) {
						shurikenEntity = new ShurikenEntity(world, playerentity, shuriken);
						shurikenEntity.shootFromRotation(playerentity, playerentity.xRot, playerentity.yRot - (10 * multishotLevel * i) + (length == 1 ? 0 : 10), 0.0F, 1.5F, 1.0F);
						if (playerentity.abilities.instabuild) {
							shurikenEntity.pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
						} else {
							count -= 1;
							if (nbt != null) {
								nbt.putInt(shurikenCount, count);
								shuriken.setTag(nbt);
							}
						}

						int k = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.PUNCH_ARROWS, shuriken);
						if (k > 0) {
							shurikenEntity.setKnockback(k);
						}

						if (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.FLAMING_ARROWS, shuriken) > 0) {
							shurikenEntity.setSecondsOnFire(100);
						}

						world.addFreshEntity(shurikenEntity);
					}
					world.playSound((PlayerEntity) null, shurikenEntity, SoundEvents.TRIDENT_THROW, SoundCategory.PLAYERS, 1.0F, 1.0F);
				}
			}
			playerentity.awardStat(Stats.ITEM_USED.get(this));
		}
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment ench) {
		return enchants.contains(ench);
	}

	@Override
	public int getEnchantmentValue() {
		return this.tier.getEnchantmentValue();
	}

	@Override
	public boolean isValidRepairItem(ItemStack item1, ItemStack item2) {
		return this.tier.getRepairIngredient().test(item2) || super.isValidRepairItem(item1, item2);
	}
}