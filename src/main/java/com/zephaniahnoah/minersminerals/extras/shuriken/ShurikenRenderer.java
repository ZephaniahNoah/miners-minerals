package com.zephaniahnoah.minersminerals.extras.shuriken;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;

// TODO: Rotation has no lerp
public class ShurikenRenderer extends EntityRenderer<ShurikenEntity> {

	public ShurikenRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public void render(ShurikenEntity spear, float float1, float float2, MatrixStack matrixStack, IRenderTypeBuffer buffer, int intIn) {
		matrixStack.pushPose();
		float rot = (float) (spear.getX() + spear.getY() - spear.getZ()) * 100f;
		matrixStack.scale(1, 1, 1);
		matrixStack.mulPose(Vector3f.YP.rotationDegrees(MathHelper.lerp(float2, spear.yRotO, spear.yRot) - 90.0F));
		matrixStack.mulPose(Vector3f.ZP.rotationDegrees(MathHelper.lerp(float2, spear.xRotO, spear.xRot) - rot));
		ItemStack itemstack = spear.getItemStack();
		if (spear.isFoil())
			itemstack.enchant(Enchantments.UNBREAKING, 1);
		IBakedModel ibakedmodel = this.itemRenderer().getModel(itemstack, spear.level, (LivingEntity) null);
		matrixStack.translate(0, -0.15, 0);

		// matrixStack.pushPose();
		this.itemRenderer().render(itemstack, ItemCameraTransforms.TransformType.GROUND, false, matrixStack, buffer, intIn, OverlayTexture.NO_OVERLAY, ibakedmodel);
		// matrixStack.popPose();

		matrixStack.popPose();
		super.render(spear, float1, float2, matrixStack, buffer, intIn);
	}

	private ItemRenderer itemRenderer() {
		return Minecraft.getInstance().getItemRenderer();
	}

	@Override
	public ResourceLocation getTextureLocation(ShurikenEntity shuriken) {
		return null;
	}
}
