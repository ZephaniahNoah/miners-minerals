package com.zephaniahnoah.minersminerals.extras.clam;

import com.mojang.datafixers.util.Pair;
import com.zephaniahnoah.minersminerals.extras.Extras;

import net.minecraft.block.BlockState;
import net.minecraft.block.TrapDoorBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.Half;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class Clam extends TrapDoorBlock {

	private static final Pair<Integer, Integer> I1 = new Pair<Integer, Integer>(14, 16);
	private static final Pair<Integer, Integer> I2 = new Pair<Integer, Integer>(10, 0);
	private static final Pair<Integer, Integer> J1 = new Pair<Integer, Integer>(10, 16);
	private static final Pair<Integer, Integer> J2 = new Pair<Integer, Integer>(7, 0);
	private static final Pair<Integer, Integer> K1 = new Pair<Integer, Integer>(7, 16);
	private static final Pair<Integer, Integer> K2 = new Pair<Integer, Integer>(4, 0);

	private final VoxelShape N;
	private final VoxelShape S;
	private final VoxelShape E;
	private final VoxelShape W;

	public Clam(Properties properties) {
		super(properties);
		N = VoxelShapes.or(getMirroredModel(true, true), BOTTOM_AABB);
		S = VoxelShapes.or(getMirroredModel(false, true), BOTTOM_AABB);
		E = VoxelShapes.or(getMirroredModel(false, false), BOTTOM_AABB);
		W = VoxelShapes.or(getMirroredModel(true, false), BOTTOM_AABB);
	}

	@Override
	public void attack(BlockState blockState, World world, BlockPos blockPos, PlayerEntity player) {
		if (player.isCrouching() && blockState.getValue(OPEN)) {
			TileEntity tileEntity = world.getBlockEntity(blockPos);
			if (tileEntity instanceof ClamTileEntity) {
				ClamTileEntity clam = ((ClamTileEntity) tileEntity);
				ItemStack clamItem = clam.getItem(0);

				// KISS Main hand only
				clam.setItem(0, player.getMainHandItem());
				player.setItemSlot(EquipmentSlotType.MAINHAND, clamItem);

			}
		}
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		BlockState blockstate = this.defaultBlockState();
		FluidState fluidstate = context.getLevel().getFluidState(context.getClickedPos());
		Direction direction = context.getClickedFace();
		if (!context.replacingClickedOnBlock() && direction.getAxis().isHorizontal()) {
			blockstate = blockstate.setValue(FACING, direction).setValue(HALF, Half.BOTTOM);
		} else {
			blockstate = blockstate.setValue(FACING, context.getHorizontalDirection().getOpposite()).setValue(HALF, Half.BOTTOM);
		}

		if (context.getLevel().hasNeighborSignal(context.getClickedPos())) {
			blockstate = blockstate.setValue(OPEN, Boolean.valueOf(true)).setValue(POWERED, Boolean.valueOf(true));
		}

		return blockstate.setValue(WATERLOGGED, Boolean.valueOf(fluidstate.getType() == Fluids.WATER));
	}

	@Override
	public VoxelShape getShape(BlockState blockState, IBlockReader blockReader, BlockPos blockPos, ISelectionContext context) {
		if (!blockState.getValue(OPEN)) {
			return blockState.getValue(HALF) == Half.TOP ? TOP_AABB : BOTTOM_AABB;
		} else {
			switch (blockState.getValue(FACING)) {
			case NORTH:
				return N;
			case SOUTH:
				return S;
			case EAST:
				return E;
			case WEST:
				return W;
			default:
				return VoxelShapes.block();
			}
		}
	}

	private static VoxelShape getMirroredModel(boolean mirrorX, boolean mirrorZ) {
		int m = mirrorX ? 1 : -1;
		int n = m == 1 ? 0 : 1;

		Pair<Integer, Integer> I1M = mirrorZ ? I1.swap() : I1;
		Pair<Integer, Integer> I2M = mirrorZ ? I2.swap() : I2;
		Pair<Integer, Integer> J1M = mirrorZ ? J1.swap() : J1;
		Pair<Integer, Integer> J2M = mirrorZ ? J2.swap() : J2;
		Pair<Integer, Integer> K1M = mirrorZ ? K1.swap() : K1;
		Pair<Integer, Integer> K2M = mirrorZ ? K2.swap() : K2;

		VoxelShape whole = VoxelShapes.or(box(I1M.getFirst() * m, 6, I1M.getSecond() * m, I2M.getFirst() * m, 0, I2M.getSecond() * m), box(J1M.getFirst() * m, 8, J1M.getSecond() * m, J2M.getFirst() * m, 6, J2M.getSecond() * m), box(K1M.getFirst() * m, 10, K1M.getSecond() * m, K2M.getFirst() * m, 8, K2M.getSecond() * m));

		return whole.move(n, 0, n);
	}

	@Override
	public void onRemove(BlockState blockState, World world, BlockPos blockPos, BlockState otherBlockState, boolean bool) {
		if (!blockState.is(otherBlockState.getBlock())) {
			TileEntity tileEntity = world.getBlockEntity(blockPos);
			if (tileEntity instanceof IInventory) {
				InventoryHelper.dropContents(world, blockPos, (IInventory) tileEntity);
				world.updateNeighbourForOutputSignal(blockPos, this);
			}
			super.onRemove(blockState, world, blockPos, otherBlockState, bool);
		}
	}

	@Override
	public boolean hasTileEntity(BlockState b) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState b, IBlockReader world) {
		return Extras.clamTileEntity.get().create();
	}
}
