package com.zephaniahnoah.minersminerals.extras.clam;

import com.zephaniahnoah.minersminerals.extras.Extras;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class ClamTileEntity extends TileEntity implements IInventory {

	private ItemStack item = ItemStack.EMPTY;
	private static final String tag = "ClamItemStack";

	public ClamTileEntity(TileEntityType<?> type) {
		super(type);
	}

	public ClamTileEntity() {
		this(Extras.clamTileEntity.get());
	}

	@Override
	public void clearContent() {
		item = ItemStack.EMPTY;
	}

	@Override
	public int getContainerSize() {
		return 1;
	}

	@Override
	public boolean isEmpty() {
		return (item == ItemStack.EMPTY || item == null);
	}

	@Override
	public ItemStack getItem(int index) {
		return item;
	}

	@Override
	public ItemStack removeItem(int slot, int count) {
		ItemStack itemStack;
		if (slot == 0) {
			if (count >= item.getCount()) {
				itemStack = item;
				clearContent();
			} else {
				item.setCount(item.getCount() - count);
				itemStack = item.copy();
				itemStack.setCount(count);
			}
		} else {
			itemStack = ItemStack.EMPTY;
		}
		markUpdated();
		return itemStack;
	}

	@Override
	public ItemStack removeItemNoUpdate(int slot) {
		return removeItem(slot, item.getCount());
	}

	@Override
	public void setItem(int slot, ItemStack itemStack) {
		item = itemStack;
		markUpdated();
	}

	@Override
	public boolean stillValid(PlayerEntity player) {
		return true;
	}

	@Override
	public boolean canPlaceItem(int slot, ItemStack itemStack) {
		return this.level.getBlockState(this.worldPosition).getValue(BlockStateProperties.OPEN);
	}

	private void markUpdated() {
		this.setChanged();
		this.getLevel().sendBlockUpdated(this.getBlockPos(), this.getBlockState(), this.getBlockState(), 3);
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt) {
		super.save(nbt);
		nbt.put(tag, this.item.serializeNBT());
		return nbt;
	}

	@Override
	public void load(BlockState b, CompoundNBT nbt) {
		super.load(b, nbt);
		this.item = ItemStack.of(nbt.getCompound(tag));
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.worldPosition, -1, getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.save(new CompoundNBT());
	}

	@Override
	public void handleUpdateTag(BlockState blockState, CompoundNBT parentNBTTagCompound) {
		this.load(blockState, parentNBTTagCompound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.load(this.level.getBlockState(pkt.getPos()), pkt.getTag());
	}
}