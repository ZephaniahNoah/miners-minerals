package com.zephaniahnoah.minersminerals.extras.clam;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3f;

public class ClamRenderer extends TileEntityRenderer<ClamTileEntity> {

	public ClamRenderer(TileEntityRendererDispatcher renderDispatcher) {
		super(renderDispatcher);
	}

	@Override
	public void render(ClamTileEntity starfish, float flo, MatrixStack matrixStack, IRenderTypeBuffer renderTypeBuffer, int combinedLightIn, int combinedOverlayIn) {
		boolean open = starfish.getBlockState().getValue(BlockStateProperties.OPEN);

		if (open) {
			Direction direction;
			if (starfish.hasLevel()) {
				direction = starfish.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING);
			} else {
				direction = Direction.NORTH;
			}

			ItemStack itemStack = starfish.getItem(0);

			matrixStack.pushPose();

			matrixStack.translate(0.5, .265, 0.5);
			matrixStack.translate(.2 * direction.getStepX(), 0, .2 * direction.getStepZ());
			matrixStack.scale(.4f, .4f, .4f);
			Direction direction1 = Direction.from2DDataValue((direction.get2DDataValue()) % 4);
			float f = -direction1.toYRot();
			matrixStack.mulPose(Vector3f.YP.rotationDegrees(f));
			matrixStack.mulPose(Vector3f.YP.rotationDegrees(180));
			IBakedModel ibakedmodel = Minecraft.getInstance().getItemRenderer().getModel(itemStack, starfish.getLevel(), (LivingEntity) null);

			matrixStack.pushPose();
			ibakedmodel = net.minecraftforge.client.ForgeHooksClient.handleCameraTransforms(matrixStack, ibakedmodel, ItemCameraTransforms.TransformType.NONE, false);

			Minecraft.getInstance().getItemRenderer().render(itemStack, ItemCameraTransforms.TransformType.FIXED, false, matrixStack, renderTypeBuffer, combinedLightIn, combinedOverlayIn, ibakedmodel);

			matrixStack.popPose();
			matrixStack.popPose();
		}
	}
}
