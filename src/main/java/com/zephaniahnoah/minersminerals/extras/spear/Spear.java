package com.zephaniahnoah.minersminerals.extras.spear;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.stats.Stats;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

// TODO: Maybe allow piercing enchant
public class Spear extends TridentItem {

	private static final List<Enchantment> enchants = (List<Enchantment>) Arrays.asList(new Enchantment[] { Enchantments.UNBREAKING, Enchantments.LOYALTY, Enchantments.MENDING, Enchantments.PUNCH_ARROWS, Enchantments.KNOCKBACK, Enchantments.FIRE_ASPECT, Enchantments.FLAMING_ARROWS });
	public static final String prismarine = "prismarine_spear";
	private final Multimap<Attribute, AttributeModifier> defaultModifiers;
	public final IItemTier tier;

	public Spear(IItemTier mineral, Item.Properties properties) {
		super(properties);
		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", (double) mineral.getAttackDamageBonus() + 2, AttributeModifier.Operation.ADDITION));
		builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Tool modifier", (double) -2.9F, AttributeModifier.Operation.ADDITION));
		this.defaultModifiers = builder.build();
		this.tier = mineral;
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slot) {
		return slot == EquipmentSlotType.MAINHAND ? this.defaultModifiers : super.getDefaultAttributeModifiers(slot);
	}

	@Override
	public void releaseUsing(ItemStack spearItemStack, World world, LivingEntity playerEntity, int use) {
		if (playerEntity instanceof PlayerEntity) {
			PlayerEntity player = (PlayerEntity) playerEntity;
			int i = this.getUseDuration(spearItemStack) - use;
			if (i >= 10) {
				int j = EnchantmentHelper.getRiptide(spearItemStack);
				if (j <= 0 || player.isInWaterOrRain()) {
					if (!world.isClientSide) {
						spearItemStack.hurtAndBreak(1, player, (plr) -> {
							plr.broadcastBreakEvent(playerEntity.getUsedItemHand());
						});
						if (j == 0) {
							SpearEntity spearEntity = new SpearEntity(world, player, spearItemStack);
							spearEntity.shootFromRotation(player, player.xRot, player.yRot, 0.0F, 2.5F + (float) j * 0.5F, 1.0F);
							if (player.abilities.instabuild) {
								spearEntity.pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
							}

							int k = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.PUNCH_ARROWS, spearItemStack);
							if (k > 0) {
								spearEntity.setKnockback(k);
							}

							if (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.FLAMING_ARROWS, spearItemStack) > 0) {
								spearEntity.setSecondsOnFire(100);
							}

							world.addFreshEntity(spearEntity);
							world.playSound((PlayerEntity) null, spearEntity, SoundEvents.TRIDENT_THROW, SoundCategory.PLAYERS, 1.0F, 1.0F);
							if (!player.abilities.instabuild) {
								player.inventory.removeItem(spearItemStack);
							}
						}
					}

					player.awardStat(Stats.ITEM_USED.get(this));
					if (j > 0) {
						float f7 = player.yRot;
						float f = player.xRot;
						float f1 = -MathHelper.sin(f7 * ((float) Math.PI / 180F)) * MathHelper.cos(f * ((float) Math.PI / 180F));
						float f2 = -MathHelper.sin(f * ((float) Math.PI / 180F));
						float f3 = MathHelper.cos(f7 * ((float) Math.PI / 180F)) * MathHelper.cos(f * ((float) Math.PI / 180F));
						float f4 = MathHelper.sqrt(f1 * f1 + f2 * f2 + f3 * f3);
						float f5 = 3.0F * ((1.0F + (float) j) / 4.0F);
						f1 = f1 * (f5 / f4);
						f2 = f2 * (f5 / f4);
						f3 = f3 * (f5 / f4);
						player.push((double) f1, (double) f2, (double) f3);
						player.startAutoSpinAttack(20);

						if (player.isOnGround())
							player.move(MoverType.SELF, new Vector3d(0.0D, (double) 1.1999999F, 0.0D));

						SoundEvent soundevent;
						if (j >= 3)
							soundevent = SoundEvents.TRIDENT_RIPTIDE_3;
						else if (j == 2)
							soundevent = SoundEvents.TRIDENT_RIPTIDE_2;
						else
							soundevent = SoundEvents.TRIDENT_RIPTIDE_1;

						world.playSound((PlayerEntity) null, player, soundevent, SoundCategory.PLAYERS, 1.0F, 1.0F);
					}
				}
			}
		}
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment ench) {
		if (((Spear) stack.getItem()).getRegistryName().getPath().equals(prismarine))
			return ench.category.canEnchant(stack.getItem());
		return enchants.contains(ench);
	}

	@Override
	public int getEnchantmentValue() {
		return this.tier.getEnchantmentValue();
	}

	@Override
	public boolean isValidRepairItem(ItemStack item1, ItemStack item2) {
		return this.tier.getRepairIngredient().test(item2) || super.isValidRepairItem(item1, item2);
	}
}
