package com.zephaniahnoah.minersminerals.extras.spear;

import com.zephaniahnoah.minersminerals.Main;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.network.NetworkHooks;

public class SpearEntity extends TridentEntity {

	private static final String defaultSpear = "copper_spear";
	private static final DataParameter<Byte> ID_LOYALTY = ObfuscationReflectionHelper.getPrivateValue(TridentEntity.class, null, "field_203053_g");// ID_LOYALTY
	private static final DataParameter<Boolean> ID_FOIL = ObfuscationReflectionHelper.getPrivateValue(TridentEntity.class, null, "field_226571_aq_");// ID_FOIL
	private static final DataParameter<String> ITEM = EntityDataManager.defineId(SpearEntity.class, DataSerializers.STRING);
	public static RegistryObject spearType;
	private String cacaheNameLookup = null;

	public SpearEntity(EntityType<?> spear, World world) {
		super((EntityType<? extends SpearEntity>) spear, world);
	}

	public SpearEntity(World world, LivingEntity thrower, ItemStack spear) {
		super((EntityType) spearType.get(), world);
		int loyalty = EnchantmentHelper.getLoyalty(spear);
		if (loyalty > 0) {
			this.setOwner(thrower);
		} else {
			this.pickup = AbstractArrowEntity.PickupStatus.ALLOWED;
		}
		this.setPos(thrower.getX(), thrower.getEyeY() - (double) 0.1F, thrower.getZ());
		ObfuscationReflectionHelper.setPrivateValue(TridentEntity.class, this, spear, "field_203054_h");// tridentItem
		this.entityData.set(ID_LOYALTY, (byte) loyalty);
		this.entityData.set(ID_FOIL, spear.hasFoil());
		this.entityData.set(ITEM, spearName(spear));
	}

	private String spearName(ItemStack spear) {
		return spear.getItem().getRegistryName().getPath();
	}

	@Override
	protected void defineSynchedData() {
		super.defineSynchedData();
		this.entityData.define(ITEM, defaultSpear);
	}

	public ItemStack getItemStack() {
		return new ItemStack(Main.spears.get(entityData.get(ITEM)));
	}

	@Override
	protected void onHitEntity(EntityRayTraceResult result) {
		Entity entity = result.getEntity();
		ItemStack spear = getPickupItem();
		float f;
		if (spear.getItem() instanceof Spear) {
			Spear item = (Spear) spear.getItem();
			f = item.tier.getAttackDamageBonus();
		} else {
			f = 2.5F;
		}
		f += 3;

		if (entity instanceof LivingEntity) {
			LivingEntity livingentity = (LivingEntity) entity;
			f += EnchantmentHelper.getDamageBonus(spear, livingentity.getMobType());
		}

		boolean flag = entity.getType() == EntityType.ENDERMAN;
		int k = entity.getRemainingFireTicks();
		if (this.isOnFire() && !flag) {
			entity.setSecondsOnFire(5);
		}

		Entity entity1 = this.getOwner();
		DamageSource damagesource = DamageSource.trident(this, (Entity) (entity1 == null ? this : entity1));
		ObfuscationReflectionHelper.setPrivateValue(TridentEntity.class, this, true, "field_203051_au");// dealtDamage
		SoundEvent soundevent = SoundEvents.TRIDENT_HIT;
		if (entity.hurt(damagesource, f)) {
			if (flag) {
				return;
			}

			int knockback = ObfuscationReflectionHelper.getPrivateValue(AbstractArrowEntity.class, this, "field_70256_ap");// knockback

			if (knockback > 0) {
				Vector3d vector3d = this.getDeltaMovement().multiply(1.0D, 0.0D, 1.0D).normalize().scale((double) knockback * 0.6D);
				if (vector3d.lengthSqr() > 0.0D) {
					entity.push(vector3d.x, 0.1D, vector3d.z);
				}
			}

			if (entity instanceof LivingEntity) {
				LivingEntity livingentity1 = (LivingEntity) entity;
				if (entity1 instanceof LivingEntity) {
					EnchantmentHelper.doPostHurtEffects(livingentity1, entity1);
					EnchantmentHelper.doPostDamageEffects((LivingEntity) entity1, livingentity1);
				}

				this.doPostHurtEffects(livingentity1);
			}
		} else {
			entity.setRemainingFireTicks(k);
		}

		this.setDeltaMovement(this.getDeltaMovement().multiply(-0.01D, -0.1D, -0.01D));
		float f1 = 1.0F;

		if (this.level instanceof ServerWorld && this.level.isThundering() && EnchantmentHelper.hasChanneling(spear)) {
			BlockPos blockpos = entity.blockPosition();
			if (this.level.canSeeSky(blockpos)) {
				LightningBoltEntity lightningboltentity = EntityType.LIGHTNING_BOLT.create(this.level);
				lightningboltentity.moveTo(Vector3d.atBottomCenterOf(blockpos));
				lightningboltentity.setCause(entity1 instanceof ServerPlayerEntity ? (ServerPlayerEntity) entity1 : null);
				this.level.addFreshEntity(lightningboltentity);
				soundevent = SoundEvents.TRIDENT_THUNDER;
				f1 = 5.0F;
			}
		}

		this.playSound(soundevent, f1, 1.0F);
	}

	@Override
	public void readAdditionalSaveData(CompoundNBT nbt) {
		super.readAdditionalSaveData(nbt);
		this.entityData.set(ITEM, nbt.getString("Spear"));
	}

	@Override
	public void addAdditionalSaveData(CompoundNBT nbt) {
		super.addAdditionalSaveData(nbt);
		nbt.putString("Spear", spearName(this.getPickupItem()));
	}

	@Override
	public IPacket<?> getAddEntityPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	protected float getWaterInertia() {
		if (cacaheNameLookup == null) {
			cacaheNameLookup = entityData.get(ITEM);
		}
		return cacaheNameLookup.equals(Spear.prismarine) ? super.getWaterInertia() : 0.6F;
	}
}
