package com.zephaniahnoah.minersminerals.extras;

public class FishFossil extends HorizontalFossilBlock {

	public FishFossil(Properties properties) {
		super(properties, box(4, 0, 2, 12, 8, 14), box(2, 0, 4, 14, 8, 12));
	}
}