package com.zephaniahnoah.minersminerals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

import javax.annotation.Nullable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.datafixers.util.Pair;
import com.zephaniahnoah.ezmodlib.ChunkDecorator;
import com.zephaniahnoah.ezmodlib.EzModLib;
import com.zephaniahnoah.ezmodlib.LootTableInjector;
import com.zephaniahnoah.ezmodlib.TagInjector;
import com.zephaniahnoah.ezmodlib.recipe.FurnaceRecipe;
import com.zephaniahnoah.ezmodlib.recipe.ShapedRecipe;
import com.zephaniahnoah.ezmodlib.recipe.ShapelessRecipe;
import com.zephaniahnoah.ezmodlib.recipe.SingleInputRecipe.SingleInputType;
import com.zephaniahnoah.ezmodlib.recipe.SmithingRecipe;
import com.zephaniahnoah.ezmodlib.util.SwapMap;
import com.zephaniahnoah.minersminerals.extras.Extras;
import com.zephaniahnoah.minersminerals.extras.clam.ClamRenderer;
import com.zephaniahnoah.minersminerals.extras.clam.ClamTileEntity;
import com.zephaniahnoah.minersminerals.extras.shuriken.Shuriken;
import com.zephaniahnoah.minersminerals.extras.shuriken.ShurikenEntity;
import com.zephaniahnoah.minersminerals.extras.shuriken.ShurikenRenderer;
import com.zephaniahnoah.minersminerals.extras.spear.Spear;
import com.zephaniahnoah.minersminerals.extras.spear.SpearEntity;
import com.zephaniahnoah.minersminerals.extras.spear.SpearRenderer;
import com.zephaniahnoah.minersminerals.extras.trex.TRexSkullRenderer;
import com.zephaniahnoah.minersminerals.extras.trex.TRexSkullTileEntity;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.OreBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType.Builder;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.inventory.EquipmentSlotType.Group;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Item;
import net.minecraft.item.Item.Properties;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTier;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.SwordItem;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tags.ItemTags;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.biome.DefaultBiomeMagnifier;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.template.RuleTest;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLEnvironment;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.registries.ForgeRegistries;

// TODO: Balancing

// TODO: Petrified forests
// TODO: Alloy forge

// TODO: Make amber block translucent
// TODO: Tinkers Construct support

@Mod(Main.MODID)
@EventBusSubscriber(modid = Main.MODID, bus = Bus.MOD)
public class Main {

	public static final String MODID = "miners-minerals";
	public static final Map<String, Item> spears = new HashMap<String, Item>();
	public static final Map<String, Item> shurikens = new HashMap<String, Item>();
	public static SwapMap<ResourceLocation, IMineralOre> baseMinerals = new SwapMap<ResourceLocation, IMineralOre>(new HashMap<ResourceLocation, IMineralOre>());
	private static List<String> dimensionBlacklist = new ArrayList<String>();
	private List<ResourceLocation> baseBlocks = new ArrayList<ResourceLocation>();
	private static final Pair<String[], String[][]> armorData = new Pair<String[], String[][]>(new String[] { "_boots", "_leggings", "_chestplate", "_helmet" }, new String[][] { //
			new String[] { //
					"# #", //
					"# #" }, //
			new String[] { //
					"###", //
					"# #", //
					"# #" }, //
			new String[] { //
					"# #", //
					"###", //
					"###" }, //
			new String[] { //
					"###", //
					"# #" } });

	public static final ItemGroup creativeTab = new ItemGroup(MODID) {
		@Override
		public ItemStack makeIcon() {
			return new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(MODID + ":adamantium_ingot")));
		}
	};

	public static final ItemGroup oreTab = new ItemGroup(MODID + ".ores") {
		@Override
		public ItemStack makeIcon() {
			return new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(MODID + ":spinel_andesite_ore")));
		}
	};

	public Main() {
		try {
			Path configFolder = Paths.get("config");
			if (!Files.exists(configFolder)) {
				Files.createDirectories(configFolder);
			}
			Path minersFolder = Paths.get("config/miners-minerals");
			if (!Files.exists(minersFolder)) {
				Files.createDirectories(minersFolder);
			}
			File config = new File("config/miners-minerals/base_blocks.txt");
			if (config.exists()) {
				Scanner reader = new Scanner(config);
				while (reader.hasNextLine()) {
					ResourceLocation loc = new ResourceLocation(reader.nextLine());
					Ores.defaultBlocks.add(loc);
					baseBlocks.add(loc);
				}
				reader.close();
			} else {
				config.createNewFile();
			}

			config = new File("config/miners-minerals/dimension_blacklist.txt");
			if (config.exists()) {
				Scanner reader = new Scanner(config);
				while (reader.hasNextLine()) {
					dimensionBlacklist.add(reader.nextLine());
				}
				reader.close();
			} else {
				config.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		FMLJavaModLoadingContext.get().getModEventBus().register(this);
		EzModLib.init(MODID);
		String name = null;
		Item i = null;

		Extras.init();
		ResourceLocation fossilName = new ResourceLocation(MODID + ":fossil_deposit_block");
		Ores.defaultBlocks.add(fossilName);
		baseMinerals.put(fossilName, Extras.ExtraMineral.FOSSIL_DEPOSIT);

		for (Mineral m : Mineral.values()) {
			String n = m.getLowerName();
			Item block = null;
			Block b = null;
			i = null;

			boolean makeItem = true;
			boolean makeBlock = true;
			boolean alloy = false;
			switch (m) {
			case RHODONITE:
			case JET:
			case SERPENTINE:
			case BLOOD_STONE:
			case CHAROITE:
			case BRONZITE:
				makeItem = false;
				break;
			case QUARTZ:
			case PRISMARINE:
				makeItem = false;
				makeBlock = false;
				break;
			case STEEL:
			case BRONZE:
			case BRASS:
				alloy = true;
				break;
			default:
				break;
			}

			if (makeBlock) {
				float destroyTime = 2f;
				float explosionResistance = 6f;
				EzModLib.register(MODID, name = n + "_block", b = new Block(AbstractBlock.Properties.of(m.gem() ? Material.STONE : Material.METAL).sound(m.gem() ? SoundType.STONE : SoundType.METAL).requiresCorrectToolForDrops().strength(destroyTime, explosionResistance)), null, i = block = new BlockItem(b, new Item.Properties().tab(creativeTab)) {
					@Override
					public int getBurnTime(ItemStack itemStack, @Nullable IRecipeType<?> recipeType) {
						if (m == Mineral.JET)
							return 1600;
						return getBurnTime(itemStack);
					}
				}, null);
				TagInjector.blocks.inject("minecraft:beacon_base_blocks", b);
				defaultBlockDrop(name);
				if (!makeItem) {
					ResourceLocation blockLoc = new ResourceLocation(MODID + ":" + name);
					baseMinerals.put(blockLoc, m);
					if (m != Mineral.BLOOD_STONE)
						Ores.defaultBlocks.add(blockLoc);
					TagInjector.blocks.inject("forge:wg_stone", b);
				}
			}

			if (makeItem) {
				EzModLib.register(MODID, name = n + (m.gem() ? "" : "_ingot"), i = new Item(new Item.Properties().tab(creativeTab)) {
					@Override
					public int getBurnTime(ItemStack itemStack, @Nullable IRecipeType<?> recipeType) {
						if (m == Mineral.AMBER)
							return 1600;
						return getBurnTime(itemStack);
					}
				});
				TagInjector.items.inject("minecraft:beacon_payment_items", i);
				if (!m.gem()) {
					Item nug;
					EzModLib.register(MODID, n + "_nugget", nug = new Item(new Item.Properties().tab(creativeTab)));
					new ShapedRecipe(new ResourceLocation(n + "_ingot_recipe"), new String[] { "###", "###", "###" }, i, 1).item('#', MODID + ":" + n + "_nugget");
					new ShapelessRecipe(new ResourceLocation(n + "_nugget_recipe"), nug, 9).item(MODID + ":" + n + "_ingot");
					TagInjector.items.inject("forge:nuggets/" + n, i);
				}
			}

			TagInjector.items.inject(m.tag, i);
//			found = false;
//
//			ITagCollection<Item> itagcollection = ItemTags.getAllTags();
//			itagcollection.getAvailableTags().stream().forEach((res) -> {
//				if (res.equals(new ResourceLocation(m.tag))) {
//					found = true;
//				}
//			});
//baseBlocks
//			if (!found) {
			ItemTags.createOptional(new ResourceLocation(m.tag));
//			}

			if (makeBlock && makeItem) {
				new ShapedRecipe(new ResourceLocation(n + "_block_recipe"), new String[] { "###", "###", "###" }, block, 1).item('#', MODID + ":" + name);
				new ShapelessRecipe(new ResourceLocation(n + "_decompress"), i, 9).item(MODID + ":" + n + "_block");
			}

			m.cook();

			for (int j = 0; j < m.baseBlocks.size(); j++) {
				ResourceLocation baseBlock = m.baseBlocks.get(j);
				if (makeBlock && makeItem && !alloy) {
					String texture;
					String blockPath;
					Block base;
					if (baseMinerals.keySet().contains(baseBlock)) {
						base = Blocks.STONE;
						blockPath = baseMinerals.get(baseBlock).getLowerName() + "_block";
						texture = MODID + ":block/" + blockPath;
					} else if (baseBlocks.contains(baseBlock)) {
						base = Blocks.STONE;
						blockPath = baseBlock.getPath();
						texture = baseBlock.getNamespace() + ":block/" + blockPath;
					} else {
						base = ForgeRegistries.BLOCKS.getValue(baseBlock);

						blockPath = baseBlock.getPath();
						if (baseBlock.equals(Blocks.BASALT.getRegistryName())) {
							texture = "minecraft:block/basalt_side";
						} else {
							texture = baseBlock.getNamespace() + ":block/" + blockPath;
						}
					}
					String blockModel = "{\"parent\":\"block/block\",\"loader\":\"forge:multi-layer\",\"layers\":{\"solid\":{\"parent\":\"block/cube_all\",\"textures\":{\"all\":\"" + //
							texture + "\"},\"transform\":{\"origin\":\"center\",\"scale\":1}},\"translucent\":{\"parent\":\"block/cube_all\",\"textures\":{\"all\":\"" + //
							MODID + ":block/" + n + "_ore\"}}},\"textures\":{\"particle\":\"" + texture + "\"}}";

					EzModLib.register(MODID, name = n + "_" + blockPath + "_ore", b = new OreBlock(AbstractBlock.Properties.copy(base).requiresCorrectToolForDrops().harvestTool(base.getHarvestTool(null)).strength(m.getLevel(), 6f).harvestLevel(m.getLevel() - 1)), blockModel, block = new BlockItem(b, new Item.Properties().tab(oreTab)), null);
					oreLootTable(m, name);
					Ores.register(m, b, j);
					new FurnaceRecipe(new ResourceLocation(m.getLowerName() + "_blast_" + blockPath), SingleInputType.BLASTING, block, i, 100, 0.7);
					new FurnaceRecipe(new ResourceLocation(m.getLowerName() + "_smelt_" + blockPath), SingleInputType.SMELTING, block, i, 200, 0.7);
				}
			}

			EzModLib.register(MODID, name = n + "_shovel", i = new ShovelItem(m, 1.5f, -3, new Item.Properties().tab(creativeTab)));
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"#", //
					"/", //
					"/" }, i, 1).tag('/', "forge:rods/wooden").tag('#', m.tag);

			EzModLib.register(MODID, name = n + "_pickaxe", i = new PickaxeItem(m, 1, -2.8f, new Item.Properties().tab(creativeTab)));
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"###", //
					" / ", //
					" / " }, i, 1).tag('/', "forge:rods/wooden").tag('#', m.tag);

			EzModLib.register(MODID, name = n + "_axe", i = new AxeItem(m, 5.5f, -3.1f, new Item.Properties().tab(creativeTab)));
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"##", //
					"#/", //
					" /" }, i, 1).tag('/', "forge:rods/wooden").tag('#', m.tag);

			EzModLib.register(MODID, name = n + "_hoe", i = new HoeItem(m, -2, -3.5f, new Item.Properties().tab(creativeTab)));
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"##", //
					" /", //
					" /" }, i, 1).tag('/', "forge:rods/wooden").tag('#', m.tag);

			EzModLib.register(MODID, name = n + "_sword", i = new SwordItem(m, 3, -2.4f, new Item.Properties().tab(creativeTab)), m == Mineral.ETHERIUM ? "{\"parent\":\"minecraft:item/handheld\",\"textures\":{\"layer0\":\"" + //
					MODID + ":item/" + n + "_sword\"},\"display\":{\"thirdperson_righthand\":{\"rotation\":[-27,-90,0],\"translation\":[0,0,-4.25]},\"thirdperson_lefthand\":{\"rotation\":[-27,90,0],\"translation\":[0,0,-4.25]},\"firstperson_righthand\":{\"rotation\":[0,-79,0]," + //
					"\"translation\":[7,0,-10.25],\"scale\":[1.1,1.1,1.1]},\"firstperson_lefthand\":{\"rotation\":[0,79,0],\"translation\":[10,0,-10.25],\"scale\":[1.1,1.1,1.1]},\"ground\":{\"rotation\":[0,0,-45],\"translation\":[0,1.5,0],\"scale\":[0.5,0.5,0.5]}}}" : null);
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"#", //
					"#", //
					"/" }, i, 1).tag('/', "forge:rods/wooden").tag('#', m.tag);

			EzModLib.register(MODID, name = n + "_greatsword", i = new SwordItem(m, 8, -3.2f, new Item.Properties().tab(creativeTab)), greatSwordModel(name));
			ShapedRecipe rec = new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"#", //
					"#", //
					"/" }, i, 1).tag('/', "forge:rods/wooden");
			if (m == Mineral.QUARTZ) {
				rec.item('#', "minecraft:quartz_block");
			} else if (m == Mineral.PRISMARINE) {
				rec.item('#', "minecraft:prismarine_bricks");
			} else if (baseMinerals.values().contains(m)) {
				rec.item('#', MODID + ":" + "polished_" + m.getLowerName());
			} else {
				rec.item('#', MODID + ":" + m.getLowerName() + "_block");
			}

			for (int k = 0; k < 4; k++) {
				EzModLib.register(MODID, name = m.getLowerName() + armorData.getFirst()[k], i = new ArmorItem(m, EquipmentSlotType.byTypeAndIndex(Group.ARMOR, k), new Item.Properties().tab(creativeTab)));
				new ShapedRecipe(new ResourceLocation(name + "_recipe"), armorData.getSecond()[k], i, 1).tag('#', m.tag);
			}

			EzModLib.register(MODID, name = n + "_spear", i = new Spear(m, new Item.Properties().tab(creativeTab).durability(m.getUses())), spearModel(name));
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					"  #", //
					" / ", //
					"/  " }, i, 1).tag('/', "forge:rods/wooden").tag('#', m.tag);
			spears.put(name, i);

			EzModLib.register(MODID, name = n + "_shuriken", i = new Shuriken(m, new Item.Properties().tab(creativeTab).durability(m.getUses())));
			new ShapedRecipe(new ResourceLocation(name + "_recipe"), new String[] { //
					" # ", //
					"# #", //
					" # " }, i, 1).tag('#', m.tag);
			shurikens.put(name, i);
		}

		registerEntities();

		// Vanilla things
		for (ItemTier tier : ItemTier.values()) {
			String n = tier.name().toLowerCase();

			Properties prop = new Item.Properties().tab(ItemGroup.TAB_COMBAT);
			if (tier == ItemTier.NETHERITE)
				prop.fireResistant();
			EzModLib.register(MODID, name = n + "_greatsword", i = new SwordItem(tier, 8, -3.2f, prop), greatSwordModel(name));
			ResourceLocation loc = new ResourceLocation(name + "_recipe");
			if (tier == ItemTier.NETHERITE) {
				Item javaIsStupidSometimes = i;
				String javaSucksName = MODID + ":" + name;
				new SmithingRecipe(loc, () -> javaSucksName, () -> "minecraft:netherite_ingot", () -> javaIsStupidSometimes.getRegistryName().toString());
			} else {
				ShapedRecipe rec = new ShapedRecipe(loc, new String[] { //
						"#", //
						"#", //
						"/" }, i, 1).tag('/', "forge:rods/wooden");

				switch (tier) {
				case WOOD:
					rec.tag('#', "minecraft:logs");
					break;
				case STONE:
					rec.tag('#', "forge:stone");
					break;
				default:
					rec.tag('#', "forge:storage_blocks/" + tier.name().toLowerCase());
					break;
				}
			}

			Properties prop2 = new Item.Properties().tab(ItemGroup.TAB_COMBAT).durability(tier.getUses());
			if (tier == ItemTier.NETHERITE)
				prop2.fireResistant();
			EzModLib.register(MODID, name = n + "_spear", i = new Spear(tier, prop2), spearModel(name));
			ResourceLocation loc2 = new ResourceLocation(name + "_recipe");
			if (tier == ItemTier.NETHERITE) {
				Item javaIsStupidSometimes = i;
				new SmithingRecipe(loc2, () -> MODID + ":diamond_spear", () -> "minecraft:netherite_ingot", () -> javaIsStupidSometimes.getRegistryName().toString());
			} else {
				ShapedRecipe rec = new ShapedRecipe(loc2, new String[] { //
						"  #", //
						" / ", //
						"/  " }, i, 1).tag('/', "forge:rods/wooden");

				switch (tier) {
				case WOOD:
					rec.tag('#', "minecraft:planks");
					break;
				case STONE:
					rec.tag('#', "forge:cobblestone").tag('#', "forge:stone");
					break;
				case DIAMOND:
					rec.tag('#', "forge:gems/diamond");
					break;
				default:
					rec.tag('#', "forge:ingots/" + tier.name().toLowerCase());
					break;
				}
			}
			spears.put(name, i);

			EzModLib.register(MODID, name = n + "_shuriken", i = new Shuriken(tier, new Item.Properties().tab(ItemGroup.TAB_COMBAT).durability(tier.getUses())));
			ResourceLocation loc3 = new ResourceLocation(name + "_recipe");
			if (tier == ItemTier.NETHERITE) {
				Item javaIsStupidSometimes = i;
				new SmithingRecipe(loc3, () -> MODID + ":diamond_shuriken", () -> "minecraft:netherite_ingot", () -> javaIsStupidSometimes.getRegistryName().toString());
			} else {
				ShapedRecipe rec = new ShapedRecipe(loc3, new String[] { //
						" # ", //
						"# #", //
						" # " }, i, 1);

				switch (tier) {
				case WOOD:
					rec.tag('#', "minecraft:planks");
					break;
				case STONE:
					rec.tag('#', "forge:cobblestone").tag('#', "forge:stone");
					break;
				case DIAMOND:
					rec.tag('#', "forge:gems/diamond");
					break;
				default:
					rec.tag('#', "forge:ingots/" + tier.name().toLowerCase());
					break;
				}
			}
			shurikens.put(name, i);
		}

		ChunkDecorator.decorators.add(new ChunkDecorator() {
			@Override
			public void decorate(IChunk chunk, Random rand) {
				if (chunk instanceof Chunk) {
					if (dimensionBlacklist.contains(((Chunk) chunk).getLevel().dimension().getRegistryName().toString())) {
						return;
					}
				}
				for (Mineral m : Mineral.values()) {
					Biome biome = DefaultBiomeMagnifier.INSTANCE.getBiome(0L, 8, 70, 8, chunk.getBiomes());
					if (m.biomes() != null) {
						boolean match = false;
						for (Category c : m.biomes()) {
							if (c.equals(biome.getBiomeCategory())) {
								match = true;
								break;
							}
						}
						if (!match) {
							continue;
						}
					}
					Block[] b = Ores.ores.get(m);
					if (b == null)
						continue;
					int veinsPerChunk = rand.nextInt(m.veinsPerChunk) + (m.veinsPerChunk / 2);
					for (int i = 0; i < veinsPerChunk; i++) {
						int x = rand.nextInt(15);
						int y = rand.nextInt(m.maxHeight - m.minHeight) + m.minHeight;
						int z = rand.nextInt(15);
						loop: for (int k = 0; k < m.size; k++) {
							Direction d = Direction.values()[rand.nextInt(Direction.values().length)];
							x += d.getStepX();
							y += d.getStepY();
							z += d.getStepZ();
							BlockPos pos = new BlockPos(x, y, z);
							ResourceLocation replacing = chunk.getBlockState(pos).getBlock().getRegistryName();
							boolean first = true;
							boolean found = false;
							for (int stoneIndex = 0; stoneIndex < m.baseBlocks.size(); stoneIndex++) {
								if (replacing.equals(m.baseBlocks.get(stoneIndex))) {
									chunk.setBlockState(pos, b[stoneIndex].defaultBlockState(), false);
									found = true;
									break;
								}
								first = false;
							}
							if (!found && first) {
								break loop;
							}
						}
					}
				}
			}
		});
	}

	public static void defaultBlockDrop(String name) {
		tableInject(name, "{\"type\":\"minecraft:block\",\"pools\":[{\"rolls\":1,\"entries\":[{\"type\":\"minecraft:item\",\"name\":\"" + MODID + ":" + name + "\"}],\"conditions\":[{\"condition\":\"minecraft:survives_explosion\"}]}]}");
	}

	private static void tableInject(String name, String table) {
		JsonObject element = new JsonParser().parse(table).getAsJsonObject();
		LootTableInjector.tablesToInject.put(new ResourceLocation(MODID, "blocks/" + name), element);
	}

	private void registerEntities() {
		Pair<Builder<?>, RegistryObject<?>> shuriken = EzModLib.register(Main.MODID, "shuriken", ShurikenEntity::new, EntityClassification.MISC);
		shuriken.getFirst().sized(0.3F, 0.3F).clientTrackingRange(4).updateInterval(20);

		Pair<Builder<?>, RegistryObject<?>> spear = EzModLib.register(Main.MODID, "spear", SpearEntity::new, EntityClassification.MISC);
		spear.getFirst().sized(0.5F, 0.5F).clientTrackingRange(4).updateInterval(20);

		if (FMLEnvironment.dist == Dist.CLIENT)
			this.registerRenders(shuriken.getSecond(), spear.getSecond());

		SpearEntity.spearType = spear.getSecond();
		ShurikenEntity.shurikenType = shuriken.getSecond();
	}

	@OnlyIn(value = Dist.CLIENT)
	private void registerRenders(RegistryObject<?> shuriken, RegistryObject<?> spear) {
		EzModLib.registerRender(ShurikenRenderer::new, "shuriken", shuriken);
		EzModLib.registerRender(SpearRenderer::new, "spear", spear);
	}

	private String spearModel(String name) {
		return "{\"parent\":\"minecraft:item/handheld\",\"textures\":{\"layer0\":\"" + //
				MODID + ":item/" + name + "\"},\"display\":{\"thirdperson_righthand\":{\"rotation\":[-90,90,0],\"translation\":[0,0,3],\"scale\":[1.625,1.625,1.625]},\"thirdperson_lefthand\":{\"rotation\":[-180,90,0],\"translation\":" + //
				"[0,0,3],\"scale\":[1.625,1.625,1.625]},\"firstperson_righthand\":{\"rotation\":[0,95,0],\"translation\":[7,0,0],\"scale\":[1.25,1.25,1.25]},\"firstperson_lefthand\":{\"rotation\":[0,-85,0],\"translation\":[7,0,0],\"scale\":[1.5,1.25,1.25]}}}";
	}

	private String greatSwordModel(String name) {
		return "{\"parent\":\"item/generated\",\"display\":{\"thirdperson_righthand\":{\"rotation\":[0,-90,55],\"translation\":[0,13.0,0],\"scale\":[1.7,1.7,0.85]}," + //
				"\"thirdperson_lefthand\":{\"rotation\":[0,90,-55],\"translation\":[0,13,0],\"scale\":[1.7,1.7,0.85]},\"firstperson_righthand\":{\"rotation\":[0,-90,25],\"translation\":[1.13,3.2,1.13],\"scale\":[1.36,1.36,0.68]},\"firstperson_lefthand\":{\"rotation\":[0,90,-25],\"translation\"" + //
				":[1.13,3.2,1.13],\"scale\":[1.36,1.36,0.68]},\"ground\":{\"rotation\":[0,0,0],\"translation\":[0,0,0],\"scale\":[1.0,1.0,0.5]},\"fixed\":{\"rotation\":[0,0,0],\"translation\":[0,0,0],\"scale\":[2.0,2.0,1.0]}},\"textures\":{\"layer0\":\"" + //
				MODID + ":item/" + name + "\"}}";
	}

	// Generates loot tables
	private void oreLootTable(Mineral m, String oreName) {
		String table;
		if (m.gem()) {
			table = "{\"type\":\"minecraft:block\",\"pools\":[{\"rolls\":1,\"entries\":[{\"type\":\"minecraft:alternatives\",\"children\":[{\"type\":\"minecraft:item\",\"conditions\":[{\"condition\":\"minecraft:match_tool\",\"predicate\":{\"enchantments\":[{\"enchantment\":\"minecraft:silk_touch\"," + //
					"\"levels\":{\"min\":1}}]}}],\"name\":\"" + MODID + ":" + oreName + "\"},{\"type\":\"minecraft:item\",\"functions\":[{\"function\":\"minecraft:set_count\",\"count\":{\"min\":" + m.minDrops + ",\"max\":" + m.maxDrops + ",\"type\":\"minecraft:uniform\"}},{\"function\":" + //
					"\"minecraft:apply_bonus\",\"enchantment\":\"minecraft:fortune\",\"formula\":\"minecraft:ore_drops\"},{\"function\":\"minecraft:explosion_decay\"}],\"name\":\"" + MODID + ":" + m.getLowerName() + "\"}]}]}]}";
		} else {
			String drop = MODID + ":";
			if (m.baseBlocks.contains(Blocks.STONE.getRegistryName())) {
				drop += m.getLowerName() + "_stone_ore";
			} else {
				drop += oreName;
			}

			table = "{\"type\":\"minecraft:block\",\"pools\":[{\"rolls\":1,\"entries\":[{\"type\":\"minecraft:alternatives\",\"children\":[{\"type\":\"minecraft:item\",\"conditions\":[{\"condition\":\"minecraft:match_tool\",\"predicate\":{\"enchantments\":[{\"enchantment\":\"minecraft:silk_touch\"," + //
					"\"levels\":{\"min\":1}}]}}],\"name\":\"" + MODID + ":" + oreName + "\"},{\"type\":\"minecraft:item\",\"conditions\":[{\"condition\":\"minecraft:survives_explosion\"}],\"name\":\"" + drop + "\"}]}]}]}";
		}
		tableInject(oreName, table);
	}

	@SuppressWarnings("unchecked")
	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public void clientSetup(FMLClientSetupEvent e) {
		// Lang
//		File f = new File("en_us.json");
//		try {
//			if (!f.exists()) {
//				f.createNewFile();
//			}
//
//			FileWriter myWriter = new FileWriter(f.getName());
//			myWriter.write("{\n");
//
//			for (ResourceLocation loc : Registry.ITEM.keySet()) {
//				Item item = ForgeRegistries.ITEMS.getValue(loc);
//				if (item == null) {
//					System.out.println(loc + " NOT REGISTERED!");
//				} else {
//					if (!loc.getNamespace().equals("minecraft")) {
//						myWriter.write("  \"" + item.getDescriptionId() + "\": \"" + toFirstCharUpperAll(loc.getPath().replaceAll("_", " ")) + "\",\n");
//					}
//				}
//			}
//
//			myWriter.write("}");
//			myWriter.close();
//
//		} catch (Exception ex) {
//
//		}

		for (Block[] ores : Ores.ores.values()) {
			for (Block block : ores) {
				net.minecraft.client.renderer.RenderTypeLookup.setRenderLayer(block, renderType -> renderType == net.minecraft.client.renderer.RenderType.solid() || renderType == net.minecraft.client.renderer.RenderType.translucent());
			}
		}
		for (Block b : Extras.transparentBlocks) {
			net.minecraft.client.renderer.RenderTypeLookup.setRenderLayer(b, net.minecraft.client.renderer.RenderType.translucent());
			net.minecraft.client.renderer.RenderTypeLookup.setRenderLayer(b, net.minecraft.client.renderer.RenderType.cutoutMipped());
		}
		ClientRegistry.bindTileEntityRenderer((TileEntityType<ClamTileEntity>) Extras.clamTileEntity.get(), ClamRenderer::new);
		ClientRegistry.bindTileEntityRenderer((TileEntityType<TRexSkullTileEntity>) Extras.trexTileEntity.get(), TRexSkullRenderer::new);
	}

	static {
		MinecraftForge.EVENT_BUS.register(new Object() {
			@SubscribeEvent
			public void onServerTick(TickEvent.ServerTickEvent e) {
				if (Shuriken.ticks >= Shuriken.lifeTime) {
					for (ServerPlayerEntity p : ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers()) {
						for (ItemStack i : p.inventory.items) {
							if (i.getItem() instanceof Shuriken) {
								CompoundNBT nbt = i.getTag();

								if (nbt == null) {
									continue;
								}

								if (nbt.contains(Shuriken.shurikenCount)) {
									int count = nbt.getInt(Shuriken.shurikenCount);
									if (count < Shuriken.stackSize) {
										nbt.putInt(Shuriken.shurikenCount, ++count);
										i.setTag(nbt);
									}
								}
							}
						}
					}
					Shuriken.ticks = 0;
				}
				Shuriken.ticks++;
			}

			@SubscribeEvent
			public void generateStoneBlobs(BiomeLoadingEvent e) {
				for (Entry<ResourceLocation, IMineralOre> p : baseMinerals.entrySet()) {
					if (p.getValue() instanceof Mineral) {
						RuleTest baseStone = OreFeatureConfig.FillerBlockType.NATURAL_STONE;
						Mineral m = (Mineral) p.getValue();
						if (m.baseBlocks.contains(Blocks.NETHERRACK.getRegistryName())) {
							baseStone = OreFeatureConfig.FillerBlockType.NETHERRACK;
						}
						boolean found = false;
						if (m.biomes() == null) {
							found = true;
						} else {
							if (Arrays.asList(m.biomes()).contains(e.getCategory())) {
								found = true;
								break;
							}
						}
						if (found) {
							ResourceLocation b = p.getKey();
							// Why? Good one Mojang.
							e.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, b, Feature.ORE.configured(new OreFeatureConfig(baseStone, Blocks.STONE.defaultBlockState(), m.size)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(m.minHeight, m.minHeight, m.maxHeight))).squared().countRandom(m.veinsPerChunk).chance(30)));
							e.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, b, Feature.ORE.configured(new OreFeatureConfig(baseStone, Blocks.STONE.defaultBlockState(), m.size / 8)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(m.minHeight, m.minHeight, m.maxHeight))).squared().count(m.veinsPerChunk)));
						}
					} else {
						// assuming Block b = p.getKey(); == Extras.fossil
						// If more underground stone blobs are added then will need to check p.getKey();
						int count = 1;
						if (Arrays.asList(p.getValue().biomes()).contains(e.getCategory())) {
							count += 1;
						}
						e.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, Extras.fossil.getRegistryName(), Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, Extras.fossil.defaultBlockState(), 45)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(20, 20, 50))).squared().count(count)));
					}
				}
			}
		});
	}
}
