package com.zephaniahnoah.minersminerals;

import net.minecraft.world.biome.Biome.Category;

public interface IMineralOre {
	public Category[] biomes();

	public String getLowerName();
}
