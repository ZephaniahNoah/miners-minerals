package com.zephaniahnoah.minersminerals;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;

public class GeneratorLanguage extends LanguageProvider {

	public GeneratorLanguage(DataGenerator gen) {
		super(gen, Main.MODID, "en_us");
	}

	@Override
	protected void addTranslations() {
		//addItem(ModItems.MININGGADGET, "Mining Gadget");
		//add("tooltop.mininggadgets.empty", "Used to craft other upgrades");
		//addBlock(ModBlocks.MINERS_LIGHT, "Miner's Light");
	}
}